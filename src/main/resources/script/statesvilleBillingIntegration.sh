#!/bin/sh

# This is the shell script that cron runs periodically.

# Here is the crontab entry:
# 15,45 * * * * /opt/flexnet/statesville/satesvilleBillingIntegration.sh 2> /opt/flexnet/statesville/logs/error.out

/bin/java -jar /opt/flexnet/statesville/JARs/statesville-billing-integration-1.0.0.jar $1 --spring.profiles.active=self061 --spring.config.location=/opt/flexnet/statesville/JARs/ >> /opt/flexnet/statesville/logs/out.log


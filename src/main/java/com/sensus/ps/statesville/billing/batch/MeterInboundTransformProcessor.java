package com.sensus.ps.statesville.billing.batch;


import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.sensus.ps.statesville.billing.model.InboundMeter;
import com.sensus.ps.statesville.billing.repository.InboundMeterRepository;


public class MeterInboundTransformProcessor
implements ItemProcessor<InboundMeter, InboundMeter> {

	/**
	 * Logs messages from MeterInboundTransformProcessor methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundTransformProcessor.class);

	/**
     * Query that selects meters for encryption processing.
     */
    private static final String METER_QUERY =
        "SELECT "
            + "m "
        + "FROM "
            + "Meter m "
        + "WHERE NOT CONTAINS "
            + "(m.meterId2, ‘OFF’)  "
        + "ORDER BY "
            + "m.id NULLS LAST, ";
    
	/**
	 * A {@code Repository} for performing {@code Meter} database operations.
	 */
	private InboundMeterRepository repository = null;
	

	@Override
	public InboundMeter process(final InboundMeter meter)    throws Exception {
    	
		//if any offpeak meters found that match with the meter id, update object with 
		// offpeak meter details (offpeakMeterId, offpeakServiceCode, offpeakSlot)
		List<InboundMeter> offpeakMeters = getMeterRepository().findByOffpeakMeterId(meter.getMeterId()+"OFF");
		logger.debug("Offpeak Meters found for {}, {}", meter.getMeterId(), offpeakMeters.size());
		
		for (Iterator iterator = offpeakMeters.iterator(); iterator.hasNext();) {
			InboundMeter offpeakMeter = (InboundMeter) iterator.next();
			meter.setOffpeakMeterId(offpeakMeter.getMeterId2());
			meter.setOffpeakServiceCode(offpeakMeter.getCustomServiceCode());
			meter.setOffpeakSlot(offpeakMeter.getCustomSlot());
		}
		
		return meter;
    }
	
	/**
	 * Returns the {@code MeterRepository} that is used to perform database
	 * operations on {@code Meter} instances.
	 *
	 * @return A {@code Repository} for performing {@code Meter} operations.
	 */
	public InboundMeterRepository getMeterRepository() {
		if (this.repository == null) {
			final String message = "Invalid/null MeterRepository";

			logger.error(message);

			throw new IllegalStateException(message);
		}

		return this.repository;
	}

	/**
	 * Provide a {@code MeterRepository} for this instance to use.
	 *
	 * @param value {@code Repository} for performing {@code Meter} operations.
	 *
	 * @return this {@code MeterFileDBWriter} instance.
	 */
	@Autowired
	private MeterInboundTransformProcessor setMeterRepository(final InboundMeterRepository value) {
		if (value == null) {
			final String message = "Invalid/null MeterRepository";

			logger.error(message);

			throw new IllegalArgumentException(message);
		}

		this.repository = value;

		return this;
	}


}

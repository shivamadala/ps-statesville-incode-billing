package com.sensus.ps.statesville.billing.batch;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.sensus.ps.statesville.billing.model.InboundMeter;
import com.sensus.ps.statesville.billing.repository.InboundMeterRepository;

public class MeterInboundTransformDBWriter 
implements ItemWriter<InboundMeter> {

	/**
	 * Logs messages from MeterFileDBWriter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundTransformDBWriter.class);

	/**
	 * A {@code Repository} for performing {@code Meter} database operations.
	 */
	private InboundMeterRepository repository = null;

	/**
	 * No-argument constructor.
	 */
	public MeterInboundTransformDBWriter() {
		super();
	}

	/**
	 * Returns the {@code MeterRepository} that is used to perform database
	 * operations on {@code Meter} instances.
	 *
	 * @return A {@code Repository} for performing {@code Meter} operations.
	 */
	public InboundMeterRepository getMeterRepository() {
		if (this.repository == null) {
			final String message = "Invalid/null MeterRepository";

			MeterInboundTransformDBWriter.logger.error(message);

			throw new IllegalStateException(message);
		}

		return this.repository;
	}

	/**
	 * Provide a {@code MeterRepository} for this instance to use.
	 *
	 * @param value {@code Repository} for performing {@code Meter} operations.
	 *
	 * @return this {@code MeterFileDBWriter} instance.
	 */
	@Autowired
	private MeterInboundTransformDBWriter setMeterRepository(final InboundMeterRepository value) {
		if (value == null) {
			final String message = "Invalid/null MeterRepository";

			MeterInboundTransformDBWriter.logger.error(message);

			throw new IllegalArgumentException(message);
		}

		this.repository = value;

		return this;
	}

	@Override
	public void write(List<? extends InboundMeter> items) throws Exception {
		for (Iterator<? extends InboundMeter> iterator = items.iterator(); iterator.hasNext();) {
			InboundMeter meter = (InboundMeter) iterator.next();
			System.out.println(meter);
			getMeterRepository().save(meter);
		}
	}
}

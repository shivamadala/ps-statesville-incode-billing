package com.sensus.ps.statesville.billing.batch;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class JobHolder {
	/**
	 * Logs messages from JobHolder methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(JobHolder.class);

	@Autowired
	private JobLauncher jobLauncher = null;

	@Autowired
	@Qualifier("job-inbound-load-transform-report")
	private Job jobInboundLoadMeter = null;

	@Autowired
	@Qualifier("job-outbound-load-transform-report")
	private Job jobOutboundLoadMeter = null;

	private boolean findArgument(final String[] candidates, final String value) {
		final String v = StringUtils.trimToEmpty(value);

		if (ArrayUtils.isEmpty(candidates) || StringUtils.isBlank(v)) {
			return false;
		}

		for (final String candidate : candidates) {
			if (StringUtils.equalsIgnoreCase(v, StringUtils.trimToEmpty(candidate))) {

				return true;
			}
		}

		return false;
	}

	private boolean findArgument(final String[] candidates, final String... values) {
		if (ArrayUtils.isEmpty(candidates) || ArrayUtils.isEmpty(values)) {
			return false;
		}

		for (final String value : values) {
			if (findArgument(candidates, value)) {
				return true;
			}
		}

		return false;
	}

	public void runJobs(final String[] arguments) {
		logger.debug("Parameters: {}", arguments);
		
		// Only run the "Inbound" Job?
		final boolean inboundJob = findArgument(arguments, "inbound", "Inbound", "InBound", "INBOUND");

		// Only run the "Outbound" Job?
		final boolean outboundJob = findArgument(arguments, "outbound", "Outbound", "OutBound", "OUTBOUND");

		if (inboundJob) {
			JobHolder.logger.info("Only run 'jobInboundLoadMeter'");
		} else if (outboundJob) {
			JobHolder.logger.info("Only run 'jobOutboundLoadMeter'");
		} else {
			JobHolder.logger.info("Please check your parameters. Allowed parameters: inbound, outbound");
		}

		final Map<String, JobParameter> map = new HashMap<>();

		map.put("time", new JobParameter(Long.valueOf(System.currentTimeMillis())));

		final JobParameters parameters = new JobParameters(map);

		// If executing inbound job or outbound job
		if (inboundJob) {

			JobHolder.logger.info("inboundJob");
			try {
				final JobExecution jobLoadMeterExecution = this.jobLauncher.run(this.jobInboundLoadMeter, parameters);

				JobHolder.logger.info("jobInboundLoadMeter: {}", jobLoadMeterExecution.getStatus());
			} catch (final Exception ex) {
				JobHolder.logger.error("An error occurred running jobInboundLoadMeter: {}", ex.toString());
			}

		} else if (outboundJob) {

			JobHolder.logger.info("outboundJob");
			try {
				final JobExecution jobLoadMeterExecution = this.jobLauncher.run(this.jobOutboundLoadMeter, parameters);

				JobHolder.logger.info("jobOutboundLoadMeter: {}", jobLoadMeterExecution.getStatus());
			} catch (final Exception ex) {
				JobHolder.logger.error("An error occurred running jobOutboundLoadMeter: {}", ex.toString());
			}

		}
	}
}

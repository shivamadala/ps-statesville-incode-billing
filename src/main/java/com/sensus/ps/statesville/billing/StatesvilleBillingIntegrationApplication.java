package com.sensus.ps.statesville.billing;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import org.springframework.context.ConfigurableApplicationContext;

import com.sensus.ps.statesville.billing.batch.JobHolder;


/**
 * The main class of this application.
 */
//Disable Spring security.
@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
public class StatesvilleBillingIntegrationApplication {
    /**
     * Logs messages from StatesvilleBillingIntegrationApplication methods.
     */
    private static final Logger logger =
        LoggerFactory.getLogger(StatesvilleBillingIntegrationApplication.class);

    /**
     * Application entry point.
     *
     * @param arguments Command-line arguments.
     *
     * @throws Exception On configuration errors, if an error occurs
     *      during execution.
     */
    public static void main(final String[] arguments)
    throws Exception {
        try (final ConfigurableApplicationContext context =
            SpringApplication.run(
                StatesvilleBillingIntegrationApplication.class,
                arguments)) {

            final JobHolder jobHolder;

            try {
                jobHolder = context.getBean("jobHolder", JobHolder.class);
            } catch (final Exception ex) {
                StatesvilleBillingIntegrationApplication.logger.error(
                    "Configuration Error: Unable to find 'jobHolder' in the "
                        + "application context: {}",
                    ex.toString());

                throw ex;
            }

            jobHolder.runJobs(arguments);
        }
    }
}

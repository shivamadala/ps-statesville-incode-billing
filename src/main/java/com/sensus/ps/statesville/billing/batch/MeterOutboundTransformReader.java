package com.sensus.ps.statesville.billing.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JpaPagingItemReader;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;

public class MeterOutboundTransformReader 
extends JpaPagingItemReader<OutboundInputMeter> {
	/**
	 * Logs messages from MeterOutboundTransformReader methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterOutboundTransformReader.class);

    /**
     * Query that selects meters for encryption processing.
     */
    private static final String METER_QUERY =
        "SELECT "
            + "m "
        + "FROM "
            + "OutboundInputMeter m ";

	/**
	 * No-argument constructor. The default reporting window is is 30-days.
	 */
	public MeterOutboundTransformReader() {
		super();
        setQueryString(METER_QUERY);

	}

	@Override
	public OutboundInputMeter read() throws Exception, UnexpectedInputException, ParseException {
		// Currently no additional functionality is required. This is behaving
		// as a "pass-through" reader would. Method implemented for debugging.
		final OutboundInputMeter meter = super.read();

		if (meter != null) {
			MeterOutboundTransformReader.logger.debug("Read meter: {}", meter);
		}

		return meter;
	}

}

package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Value;

import com.sensus.ps.statesville.billing.model.OutboundMeter;

/**
 * This is an {@code ItemWriter} that updates {@code OutboundMeter} records that
 * are in the disable-encryption stage of the overall life-cycle.
 */
public class MeterOutboundReportWriter extends FlatFileItemWriter<List<OutboundMeter>> {
	/**
	 * Logs messages from MeterDisableEncryptionDBWriter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterOutboundReportWriter.class);

	private static SimpleDateFormat format = null;

	static {
		MeterOutboundReportWriter.format = new SimpleDateFormat("yyyyMMddHHmmss");
		MeterOutboundReportWriter.format.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
	}

	private String path = "";

	/**
	 * No-argument constructor.
	 */
	public MeterOutboundReportWriter() {
		super();
	}

	private synchronized String asString(final Date value) {
		return (value == null) ? "" : MeterOutboundReportWriter.format.format(value);
	}

	public String getOutFilePath() {
		if (StringUtils.isBlank(this.path)) {
			throw new IllegalStateException("Invalid/blank output file path");
		}

		return this.path;
	}

	@Value("${ps.statesville.client.output.directory}")
	public MeterOutboundReportWriter setOutFilePath(final String value) {

		final String v = StringUtils.trimToEmpty(value);

		if (StringUtils.isBlank(v)) {
			throw new IllegalArgumentException("Invalid/blank output file path");
		}

		if (!StringUtils.endsWith(v, "/") && !StringUtils.endsWith(v, "\\")) {
			this.path = v + "/";
		} else {
			this.path = v;
		}

		return this;
	}

	private FileWriter createOutFile() throws RuntimeException {

		final String outpath = getOutFilePath();

		if (StringUtils.isBlank(outpath)) {
			throw new IllegalArgumentException("No output path specified");
		}

		try {
			final String targetFilePath = outpath + "Billing_Meter_Export_"
					+ MeterOutboundReportWriter.format.format(new Date()) + ".txt";

			logger.info("Creating meter report file: '{}'", targetFilePath);

			final File outFile = new File(targetFilePath);
			final File targetDirectory = outFile.getParentFile();

			if ((targetDirectory != null) && !targetDirectory.exists()) {
				logger.info("Creating meter report parent directory: '{}'", targetDirectory.getAbsolutePath());

				targetDirectory.mkdirs();
			}

			return new FileWriter(outFile, true);
		} catch (final Exception ex) {
			logger.error("Unable to create output file: '{}'", ex.toString());
		}

		return null;
	}

	public MeterOutboundReportWriter outputMeters(final List<? extends List<OutboundMeter>> itemList) throws Exception {

		int metersWrittenCount = 0;

		try (final FileWriter writer = createOutFile()) {

			if (writer == null) {
				throw new IllegalStateException("Unable to create writer; cannot write output file");
			}

			// Check if there are any records to write after file has been
			// created. This is to make sure a file is created even if there
			// is nothing to report.
			if (isEmpty(itemList) || isEmpty(itemList.get(0))) {

				logger.info("No Meters provided");

				writer.flush();

				return this;
			}

			for (List<OutboundMeter> items : itemList) {

				for (final OutboundMeter item : items) {

					if (item == null) {
						continue;
					}

					// output string
					writer.write(String.format("%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",

							// Common to all output lines
							StringUtils.rightPad(item.getAccountId(), 12),

							"  ",

							StringUtils.rightPad(item.getServiceCode(), 3),
							StringUtils.rightPad(item.getServiceSlot(), 2),

							" ", "D",

							StringUtils.rightPad(item.getNonTouKwhRead(), 9),
							StringUtils.rightPad(item.getNonTouKwhReadDate(), 6),
							StringUtils.rightPad(item.getNonTouKwhReadTime(), 6),

							StringUtils.rightPad(item.getIncodeLatitude(), 11),
							StringUtils.rightPad(item.getIncodeLongitude(), 11),

							StringUtils.rightPad(item.getNonTouKwRead(), 9),
							StringUtils.rightPad(item.getNonTouKwReadDate(), 6),
							StringUtils.rightPad(item.getNonTouKwReadTime(), 6),

							StringUtils.rightPad(item.getTouTier1Read(), 9),
							StringUtils.rightPad(item.getTouTier1ReadDate(), 6),
							StringUtils.rightPad(item.getTouTier1ReadTime(), 6)));

					// writer.write(item.getOutputString()+"\r\n");

					logger.debug("Meter CSV record written: {}", item);
				}

				metersWrittenCount += items.size();

				writer.flush();

			}

		} catch (final Exception ex) {
			logger.error("Unable to write output file: {}", ex.toString());
		}

		logger.info("Output of {} complete", metersWrittenCount);

		return this;
	}

	/*
	 * public static String fixedLengthString(String string, int length) { return
	 * String.format("%1$"+length+ "s", string); }
	 */

	public void write(final List<? extends List<OutboundMeter>> items) throws Exception {
		outputMeters(items);
	}
}

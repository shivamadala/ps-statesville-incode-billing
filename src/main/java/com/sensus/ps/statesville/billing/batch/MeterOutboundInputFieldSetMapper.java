package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.lang3.StringUtils.isBlank;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;

import ch.qos.logback.classic.Logger;

public class MeterOutboundInputFieldSetMapper implements FieldSetMapper<OutboundInputMeter> {
	@Override
	public OutboundInputMeter mapFieldSet(final FieldSet fieldSet) throws BindException {
		if ((fieldSet == null) || (fieldSet.getFieldCount() < 1)) {
			return null;
		}

		final String accountId = fieldSet.readString("accountId");

		if (isBlank(accountId)) {
			return null;
		}

		final OutboundInputMeter meter = new OutboundInputMeter();

		meter.setAccountId(fieldSet.readString("accountId"));
		
		meter.setBlank1(fieldSet.readString("blank1"));
		
		meter.setCustomServiceCode(fieldSet.readString("customServiceCode"));
		meter.setCustomSlot(fieldSet.readString("customSlot"));
		
		meter.setOffpeakSlot(fieldSet.readString("offpeakSlot"));
		meter.setOffpeakServiceCode(fieldSet.readString("offpeakServiceCode"));
		
		meter.setBlank2(fieldSet.readString("blank2"));
		meter.setBlank3(fieldSet.readString("blank3"));

		meter.setKwhRead(fieldSet.readString("kwhRead"));
		meter.setKwhReadDate(fieldSet.readString("kwhReadDate"));
		meter.setKwhReadTime(fieldSet.readString("kwhReadTime"));
		
		meter.setCustomIncodeLatitude(fieldSet.readString("customIncodeLatitude"));
		meter.setCustomIncodeLongitude(fieldSet.readString("customIncodeLongitude"));
		
		meter.setKwRead(fieldSet.readString("kwRead"));
		meter.setKwReadDate(fieldSet.readString("kwReadDate"));
		meter.setKwReadTime(fieldSet.readString("kwReadTime"));
		
		meter.setTierAkwhRead(isBlank(fieldSet.readString("tierAkwhRead")) || "0".equals(fieldSet.readString("tierAkwhRead")) ? fieldSet.readString("kwhRead")
				: fieldSet.readString("tierAkwhRead"));
		meter.setTierAkwhReadDate(isBlank(fieldSet.readString("tierAkwhReadDate")) || "0".equals(fieldSet.readString("tierAkwhReadDate")) ? fieldSet.readString("kwhReadDate")
				: fieldSet.readString("tierAkwhReadDate"));
		meter.setTierAkwhReadTime(isBlank(fieldSet.readString("tierAkwhReadTime")) || "0".equals(fieldSet.readString("tierAkwhReadTime")) ? fieldSet.readString("kwhReadTime")
				: fieldSet.readString("tierAkwhReadTime"));
		
		meter.setTierAkwRead(isBlank(fieldSet.readString("tierAkwRead")) ? fieldSet.readString("kwRead")
				: fieldSet.readString("tierAkwRead"));
		meter.setTierAkwReadDate(isBlank(fieldSet.readString("tierAkwReadDate")) ? fieldSet.readString("kwReadDate")
				: fieldSet.readString("tierAkwReadDate"));
		meter.setTierAkwReadTime(isBlank(fieldSet.readString("tierAkwReadTime")) ? fieldSet.readString("kwReadTime")
				: fieldSet.readString("tierAkwReadTime"));
		
		meter.setTierApfRead(fieldSet.readString("tierApfRead"));
		meter.setTierApfReadDate(fieldSet.readString("tierApfReadDate"));
		meter.setTierApfReadTime(fieldSet.readString("tierApfReadTime"));
		
		meter.setTierBkwhRead(fieldSet.readString("tierBkwhRead"));
		meter.setTierBkwhReadDate(fieldSet.readString("tierBkwhReadDate"));
		meter.setTierBkwhReadTime(fieldSet.readString("tierBkwhReadTime"));
		
		meter.setTierBkwRead(fieldSet.readString("tierBkwRead"));
		meter.setTierBkwReadDate(fieldSet.readString("tierBkwReadDate"));
		meter.setTierBkwReadTime(fieldSet.readString("tierBkwReadTime"));

		return meter;
	}
}

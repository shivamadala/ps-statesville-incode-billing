package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Value;

import com.sensus.ps.statesville.billing.model.InboundMeter;

/**
 * This is an {@code ItemWriter} that updates {@code Meter} records that are in
 * the disable-encryption stage of the overall life-cycle.
 */
public class MeterInboundReportWriter 
extends FlatFileItemWriter<InboundMeter> {
	/**
	 * Logs messages from MeterDisableEncryptionDBWriter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundReportWriter.class);

	private static SimpleDateFormat format = null;

	static {
		MeterInboundReportWriter.format = new SimpleDateFormat("yyyyMMddHHmmss");
		MeterInboundReportWriter.format.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
	}

	private String path = "";

	/**
	 * No-argument constructor.
	 */
	public MeterInboundReportWriter() {
		super();
	}

	private synchronized String asString(final Date value) {
		return (value == null) ? "" : MeterInboundReportWriter.format.format(value);
	}

	public String getOutFilePath() {
		if (StringUtils.isBlank(this.path)) {
			throw new IllegalStateException("Invalid/blank output file path");
		}

		return this.path;
	}

	@Value("${ps.statesville.client.output.directory}")
	public MeterInboundReportWriter setOutFilePath(final String value) {
		final String v = StringUtils.trimToEmpty(value);

		if (StringUtils.isBlank(v)) {
			throw new IllegalArgumentException("Invalid/blank output file path");
		}

		if (!StringUtils.endsWith(v, "/") && !StringUtils.endsWith(v, "\\")) {
			this.path = v + "/";
		} else {
			this.path = v;
		}

		return this;
	}

	private FileWriter createOutFile() throws RuntimeException {
		final String outpath = getOutFilePath();

		if (StringUtils.isBlank(outpath)) {
			throw new IllegalArgumentException("No output path specified");
		}
		
		try {
			final String targetFilePath = outpath + "STSVL_VFLEX_WE_" + MeterInboundReportWriter.format.format(new Date()) + ".csv";

			logger.info("Creating meter report file: '{}'", targetFilePath);

			final File outFile = new File(targetFilePath);
			final File targetDirectory = outFile.getParentFile();

			if ((targetDirectory != null) && !targetDirectory.exists()) {
				logger.info("Creating meter report parent directory: '{}'", targetDirectory.getAbsolutePath());

				targetDirectory.mkdirs();
			}

			return new FileWriter(outFile, true);
		} catch (final Exception ex) {
			logger.error("Unable to create output file: '{}'", ex.toString());
		}

		return null;
	}

	public MeterInboundReportWriter outputMeters(final List<? extends InboundMeter> items) throws Exception {
		try (final FileWriter writer = createOutFile()) {

			if (writer == null) {
				throw new IllegalStateException("Unable to create writer; cannot write output file");
			}

			// Check if there are any records to write after file has been
			// created. This is to make sure a file is created even if there
			// is nothing to report.
			if (isEmpty(items)) {
				logger.info("No Meters provided");

				writer.flush();

				return this;
			}
			
			for (final InboundMeter item : items) {
				if (item == null) {
					continue;
				}

				// skip the line if this is offpeak meter
				if (item.getMeterId2().contains("OFF")) {
					continue;
				}

				// output string
				writer.write(item.getOutputString()+"\r\n");

				logger.debug("Meter CSV record written: {}", item);
			}

			writer.flush();

			logger.debug("Meter report written");

		} catch (final Exception ex) {
			logger.error("Unable to write output file: {}", ex.toString());
		}

		logger.info("Output of {} complete", items.size());

		return this;
	}

	@Override
	public void write(final List<? extends InboundMeter> items) throws Exception {
		logger.debug("Chunk Size {}", items.size());
		outputMeters(items);
	}
}

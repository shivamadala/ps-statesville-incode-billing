package com.sensus.ps.statesville.billing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "OutboundMeter")
public class OutboundMeter {

	/**
	 * Logs messages from Meter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(OutboundMeter.class);

	/**
	 * The object-identifier (OID) for this persistable domain object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outboundMeterIDGenerator")
	@SequenceGenerator(name = "outboundMeterIDGenerator", sequenceName = "OUTBOUNDMETERIDSEQ", allocationSize = 1)
	@Column(name = "ID", updatable = false, nullable = false)
	private Long id = null;

	/**
	 * The version for this persistable domain object. Can be considered the number
	 * of times this given instance has been written to the data-store.
	 */
	@Version
	private long version = 0;

	@Column(name = "ACCOUNTID", length = 15)
	private String accountId = null;

	@Column(name = "SERVICECODE", length = 3)
	private String serviceCode = null;

	@Column(name = "SERVICESLOT", length = 1)
	private String serviceSlot = null;

	@Column(name = "NONTOUKWHREAD", length = 10)
	private String nonTouKwhRead = null;

	@Column(name = "NONTOUKWHREADDATE", length = 6)
	private String nonTouKwhReadDate = null;

	@Column(name = "NONTOUKWHREADTIME", length = 6)
	private String nonTouKwhReadTime = null;

	@Column(name = "INCODELATITUDE", length = 10)
	private String incodeLatitude = null;

	@Column(name = "INCODELONGITUDE", length = 10)
	private String incodeLongitude = null;

	@Column(name = "NONTOUKWREAD", length = 10)
	private String nonTouKwRead = null;

	@Column(name = "NONTOUKWREADDATE", length = 6)
	private String nonTouKwReadDate = null;

	@Column(name = "NONTOUKWREADTIME", length = 6)
	private String nonTouKwReadTime = null;

	@Column(name = "TOUTIER1READ", length = 10)
	private String touTier1Read = null;

	@Column(name = "TOUTIER1READDATE", length = 6)
	private String touTier1ReadDate = null;

	@Column(name = "TOUTIER1READTIME", length = 6)
	private String touTier1ReadTime = null;

	public OutboundMeter() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}

	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	/**
	 * @return the serviceSlot
	 */
	public String getServiceSlot() {
		return serviceSlot;
	}

	/**
	 * @param serviceSlot the serviceSlot to set
	 */
	public void setServiceSlot(String serviceSlot) {
		this.serviceSlot = serviceSlot;
	}

	/**
	 * @return the nonTouKwhRead
	 */
	public String getNonTouKwhRead() {
		return nonTouKwhRead;
	}

	/**
	 * @param nonTouKwhRead the nonTouKwhRead to set
	 */
	public void setNonTouKwhRead(String nonTouKwhRead) {
		this.nonTouKwhRead = nonTouKwhRead;
	}

	/**
	 * @return the nonTouKwhReadDate
	 */
	public String getNonTouKwhReadDate() {
		return nonTouKwhReadDate;
	}

	/**
	 * @param nonTouKwhReadDate the nonTouKwhReadDate to set
	 */
	public void setNonTouKwhReadDate(String nonTouKwhReadDate) {
		this.nonTouKwhReadDate = nonTouKwhReadDate;
	}

	/**
	 * @return the nonTouKwhReadTime
	 */
	public String getNonTouKwhReadTime() {
		return nonTouKwhReadTime;
	}

	/**
	 * @param nonTouKwhReadTime the nonTouKwhReadTime to set
	 */
	public void setNonTouKwhReadTime(String nonTouKwhReadTime) {
		this.nonTouKwhReadTime = nonTouKwhReadTime;
	}

	/**
	 * @return the incodeLatitude
	 */
	public String getIncodeLatitude() {
		return incodeLatitude;
	}

	/**
	 * @param incodeLatitude the incodeLatitude to set
	 */
	public void setIncodeLatitude(String incodeLatitude) {
		this.incodeLatitude = incodeLatitude;
	}

	/**
	 * @return the incodeLongitude
	 */
	public String getIncodeLongitude() {
		return incodeLongitude;
	}

	/**
	 * @param incodeLongitude the incodeLongitude to set
	 */
	public void setIncodeLongitude(String incodeLongitude) {
		this.incodeLongitude = incodeLongitude;
	}

	/**
	 * @return the nonTouKwRead
	 */
	public String getNonTouKwRead() {
		return nonTouKwRead;
	}

	/**
	 * @param nonTouKwRead the nonTouKwRead to set
	 */
	public void setNonTouKwRead(String nonTouKwRead) {
		this.nonTouKwRead = nonTouKwRead;
	}

	/**
	 * @return the nonTouKwReadDate
	 */
	public String getNonTouKwReadDate() {
		return nonTouKwReadDate;
	}

	/**
	 * @param nonTouKwReadDate the nonTouKwReadDate to set
	 */
	public void setNonTouKwReadDate(String nonTouKwReadDate) {
		this.nonTouKwReadDate = nonTouKwReadDate;
	}

	/**
	 * @return the nonTouKwReadTime
	 */
	public String getNonTouKwReadTime() {
		return nonTouKwReadTime;
	}

	/**
	 * @param nonTouKwReadTime the nonTouKwReadTime to set
	 */
	public void setNonTouKwReadTime(String nonTouKwReadTime) {
		this.nonTouKwReadTime = nonTouKwReadTime;
	}

	/**
	 * @return the touTier1Read
	 */
	public String getTouTier1Read() {
		return touTier1Read;
	}

	/**
	 * @param touTier1Read the touTier1Read to set
	 */
	public void setTouTier1Read(String touTier1Read) {
		this.touTier1Read = touTier1Read;
	}

	/**
	 * @return the touTier1ReadDate
	 */
	public String getTouTier1ReadDate() {
		return touTier1ReadDate;
	}

	/**
	 * @param touTier1ReadDate the touTier1ReadDate to set
	 */
	public void setTouTier1ReadDate(String touTier1ReadDate) {
		this.touTier1ReadDate = touTier1ReadDate;
	}

	/**
	 * @return the touTier1ReadTime
	 */
	public String getTouTier1ReadTime() {
		return touTier1ReadTime;
	}

	/**
	 * @param touTier1ReadTime the touTier1ReadTime to set
	 */
	public void setTouTier1ReadTime(String touTier1ReadTime) {
		this.touTier1ReadTime = touTier1ReadTime;
	}

	public void populateOnPeakMeter(OutboundInputMeter item) {
		this.accountId = item.getAccountId();
		this.serviceCode = item.getCustomServiceCode();
		this.serviceSlot = item.getCustomSlot();
		this.nonTouKwhRead = item.getTierAkwhRead();
		this.nonTouKwhReadDate = item.getTierAkwhReadDate();
		this.nonTouKwhReadTime = item.getTierAkwhReadTime();
		this.incodeLatitude = item.getIncodeLatitude();
		this.incodeLongitude = item.getIncodeLongitude();
		this.nonTouKwRead = item.getTierAkwRead();
		this.nonTouKwReadDate = item.getTierAkwReadDate();
		this.nonTouKwReadTime = item.getTierAkwReadTime();
		this.touTier1Read = item.getTierApfRead();
		this.touTier1ReadDate = item.getTierApfReadDate();
		this.touTier1ReadTime = item.getTierApfReadTime();
	}

	public void populateOffPeakMeter(OutboundInputMeter item) {
		this.accountId = item.getAccountId();
		this.serviceCode = item.getOffpeakServiceCode();
		this.serviceSlot = item.getOffpeakSlot();
		this.nonTouKwhRead = item.getTierBkwhRead();
		this.nonTouKwhReadDate = item.getTierBkwhReadDate();
		this.nonTouKwhReadTime = item.getTierBkwhReadTime();
		this.incodeLatitude = item.getIncodeLatitude();
		this.incodeLongitude = item.getIncodeLongitude();
		this.nonTouKwRead = item.getTierBkwRead();
		this.nonTouKwReadDate = item.getTierBkwReadDate();
		this.nonTouKwReadTime = item.getTierBkwReadTime();
	}

	/*
	 * @Override public String toString() { return "MeterDetail [" + (getFlexNetId()
	 * != null ? "getFlexNetId()=" + getFlexNetId() + ", " : "") + (getMeterId() !=
	 * null ? "getMeterId()=" + getMeterId() + ", " : "") + (getServiceType() !=
	 * null ? "getServiceType()=" + getServiceType() + ", " : "") + (getDeviceType()
	 * != null ? "getDeviceType()=" + getDeviceType() + ", " : "") +
	 * (getSampleRate() != null ? "getSampleRate()=" + getSampleRate() + ", " : "")
	 * + (getCustomerId() != null ? "getCustomerId()=" + getCustomerId() : "") +
	 * "]"; }
	 */

	public String getOutputString() {
		return accountId + ",," + serviceCode + "," + serviceSlot + ",,D," + nonTouKwhRead + "," + nonTouKwhReadDate + ","
				+ nonTouKwhReadTime + "," + incodeLatitude + "," + incodeLongitude + "," + nonTouKwRead + ","
				+ nonTouKwReadDate + "," + nonTouKwReadTime + "," + touTier1Read + "," + touTier1ReadDate + ","
				+ touTier1ReadTime;
	}

}

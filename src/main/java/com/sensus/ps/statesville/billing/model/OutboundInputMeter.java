package com.sensus.ps.statesville.billing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "OutboundInputMeter")
public class OutboundInputMeter {

	/**
	 * Logs messages from Meter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(OutboundInputMeter.class);

	/**
	 * The object-identifier (OID) for this persistable domain object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "outboundInputMeterIDGenerator")
	@SequenceGenerator(name = "outboundInputMeterIDGenerator", sequenceName = "OUTBOUNDINPUTMETERIDSEQ", allocationSize = 1)
	@Column(name = "ID", updatable = false, nullable = false)
	private Long id = null;

	/**
	 * The version for this persistable domain object. Can be considered the number
	 * of times this given instance has been written to the data-store.
	 */
	@Version
	private long version = 0;

	@Column(name = "ACCOUNTID", length = 15)
	private String accountId = null;
	
	private String blank1 = "";
	
	@Column(name = "CUSTOMSERVICECODE", length = 3)
	private String customServiceCode = null;

	@Column(name = "CUSTOMSLOT", length = 3)
	private String customSlot = null;

	@Column(name = "OFFPEAKSLOT", length = 3)
	private String offpeakSlot = null;

	@Column(name = "OFFPEAKSERVICECODE", length = 3)
	private String offpeakServiceCode = null;

	private String blank2 = "";
	
	private String blank3 = "D";
	
	@Column(name = "KWHREAD", length = 9)
	private String kwhRead = null;

	@Column(name = "KWHREADDATE", length = 6)
	private String kwhReadDate = null;

	@Column(name = "KWHREATIME", length = 6)
	private String kwhReadTime = null;

	@Column(name = "CUSTOMINCODELATITUDE", length = 11)
	private String customIncodeLatitude = null;

	@Column(name = "CUSTOMINCODELONGITUDE", length = 11)
	private String customIncodeLongitude = null;

	@Column(name = "KWREAD", length = 9)
	private String kwRead = null;

	@Column(name = "KWREADDATE", length = 6)
	private String kwReadDate = null;

	@Column(name = "KWREADTIME", length = 6)
	private String kwReadTime = null;

	@Column(name = "TIERAKWHREAD", length = 9)
	private String tierAkwhRead = null;

	@Column(name = "TIERAKWHREADDATE", length = 6)
	private String tierAkwhReadDate = null;

	@Column(name = "TIERAKWHREADTIME", length = 6)
	private String tierAkwhReadTime = null;

	@Column(name = "TIERAKWREAD", length = 9)
	private String tierAkwRead = null;

	@Column(name = "TIERAKWREADDATE", length = 6)
	private String tierAkwReadDate = null;

	@Column(name = "TIERAKWREADTIME", length = 6)
	private String tierAkwReadTime = null;

	@Column(name = "TIERAPFREAD", length = 9)
	private String tierApfRead = null;

	@Column(name = "TIERAPFREADDATE", length = 6)
	private String tierApfReadDate = null;

	@Column(name = "TIERAPFREADTIME", length = 6)
	private String tierApfReadTime = null;

	@Column(name = "TIERBKWHREAD", length = 9)
	private String tierBkwhRead = null;

	@Column(name = "TIERBKWHREADDATE", length = 6)
	private String tierBkwhReadDate = null;

	@Column(name = "TIERBKWHREADTIME", length = 6)
	private String tierBkwhReadTime = null;

	@Column(name = "TIERBKWREAD", length = 9)
	private String tierBkwRead = null;

	@Column(name = "TIERBKWREADDATE", length = 6)
	private String tierBkwReadDate = null;

	@Column(name = "TIERBKWREADTIME", length = 6)
	private String tierBkwReadTime = null;

	public OutboundInputMeter() {
		super();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the blank1
	 */
	public String getBlank1() {
		return blank1;
	}

	/**
	 * @param blank1 the blank1 to set
	 */
	public void setBlank1(String blank1) {
		this.blank1 = blank1;
	}

	/**
	 * @return the customServiceCode
	 */
	public String getCustomServiceCode() {
		return customServiceCode;
	}

	/**
	 * @param customServiceCode the customServiceCode to set
	 */
	public void setCustomServiceCode(String customServiceCode) {
		this.customServiceCode = customServiceCode;
	}

	/**
	 * @return the customSlot
	 */
	public String getCustomSlot() {
		return customSlot;
	}

	/**
	 * @param customSlot the customSlot to set
	 */
	public void setCustomSlot(String customSlot) {
		this.customSlot = customSlot;
	}

	/**
	 * @return the offpeakSlot
	 */
	public String getOffpeakSlot() {
		return offpeakSlot;
	}

	/**
	 * @param offpeakSlot the offpeakSlot to set
	 */
	public void setOffpeakSlot(String offpeakSlot) {
		this.offpeakSlot = offpeakSlot;
	}

	/**
	 * @return the offpeakServiceCode
	 */
	public String getOffpeakServiceCode() {
		return offpeakServiceCode;
	}

	/**
	 * @param offpeakServiceCode the offpeakServiceCode to set
	 */
	public void setOffpeakServiceCode(String offpeakServiceCode) {
		this.offpeakServiceCode = offpeakServiceCode;
	}

	/**
	 * @return the blank2
	 */
	public String getBlank2() {
		return blank2;
	}

	/**
	 * @param blank2 the blank2 to set
	 */
	public void setBlank2(String blank2) {
		this.blank2 = blank2;
	}

	/**
	 * @return the blank3
	 */
	public String getBlank3() {
		return blank3;
	}

	/**
	 * @param blank3 the blank3 to set
	 */
	public void setBlank3(String blank3) {
		this.blank3 = blank3;
	}

	/**
	 * @return the kwhRead
	 */
	public String getKwhRead() {
		return kwhRead;
	}

	/**
	 * @param kwhRead the kwhRead to set
	 */
	public void setKwhRead(String kwhRead) {
		this.kwhRead = kwhRead;
	}

	/**
	 * @return the kwhReadDate
	 */
	public String getKwhReadDate() {
		return kwhReadDate;
	}

	/**
	 * @param kwhReadDate the kwhReadDate to set
	 */
	public void setKwhReadDate(String kwhReadDate) {
		this.kwhReadDate = kwhReadDate;
	}

	/**
	 * @return the kwhReadTime
	 */
	public String getKwhReadTime() {
		return kwhReadTime;
	}

	/**
	 * @param kwhReadTime the kwhReadTime to set
	 */
	public void setKwhReadTime(String kwhReadTime) {
		this.kwhReadTime = kwhReadTime;
	}

	/**
	 * @return the customIncodeLatitude
	 */
	public String getIncodeLatitude() {
		return customIncodeLatitude;
	}

	/**
	 * @param customIncodeLatitude the customIncodeLatitude to set
	 */
	public void setCustomIncodeLatitude(String customIncodeLatitude) {
		this.customIncodeLatitude = customIncodeLatitude;
	}

	/**
	 * @return the customIncodeLongitude
	 */
	public String getIncodeLongitude() {
		return customIncodeLongitude;
	}

	/**
	 * @param customIncodeLongitude the customIncodeLongitude to set
	 */
	public void setCustomIncodeLongitude(String customIncodeLongitude) {
		this.customIncodeLongitude = customIncodeLongitude;
	}

	/**
	 * @return the kwRead
	 */
	public String getKwRead() {
		return kwRead;
	}

	/**
	 * @param kwRead the kwRead to set
	 */
	public void setKwRead(String kwRead) {
		this.kwRead = kwRead;
	}

	/**
	 * @return the kwReadDate
	 */
	public String getKwReadDate() {
		return kwReadDate;
	}

	/**
	 * @param kwReadDate the kwReadDate to set
	 */
	public void setKwReadDate(String kwReadDate) {
		this.kwReadDate = kwReadDate;
	}

	/**
	 * @return the kwReadTime
	 */
	public String getKwReadTime() {
		return kwReadTime;
	}

	/**
	 * @param kwReadTime the kwReadTime to set
	 */
	public void setKwReadTime(String kwReadTime) {
		this.kwReadTime = kwReadTime;
	}

	/**
	 * @return the tierAkwhRead
	 */
	public String getTierAkwhRead() {
		return tierAkwhRead;
	}

	/**
	 * @param tierAkwhRead the tierAkwhRead to set
	 */
	public void setTierAkwhRead(String tierAkwhRead) {
		this.tierAkwhRead = tierAkwhRead;
	}

	/**
	 * @return the tierAkwhReadDate
	 */
	public String getTierAkwhReadDate() {
		return tierAkwhReadDate;
	}

	/**
	 * @param tierAkwhReadDate the tierAkwhReadDate to set
	 */
	public void setTierAkwhReadDate(String tierAkwhReadDate) {
		this.tierAkwhReadDate = tierAkwhReadDate;
	}

	/**
	 * @return the tierAkwhReadTime
	 */
	public String getTierAkwhReadTime() {
		return tierAkwhReadTime;
	}

	/**
	 * @param tierAkwhReadTime the tierAkwhReadTime to set
	 */
	public void setTierAkwhReadTime(String tierAkwhReadTime) {
		this.tierAkwhReadTime = tierAkwhReadTime;
	}

	/**
	 * @return the tierAkwRead
	 */
	public String getTierAkwRead() {
		return tierAkwRead;
	}

	/**
	 * @param tierAkwRead the tierAkwRead to set
	 */
	public void setTierAkwRead(String tierAkwRead) {
		this.tierAkwRead = tierAkwRead;
	}

	/**
	 * @return the tierAkwReadDate
	 */
	public String getTierAkwReadDate() {
		return tierAkwReadDate;
	}

	/**
	 * @param tierAkwReadDate the tierAkwReadDate to set
	 */
	public void setTierAkwReadDate(String tierAkwReadDate) {
		this.tierAkwReadDate = tierAkwReadDate;
	}

	/**
	 * @return the tierAkwReadTime
	 */
	public String getTierAkwReadTime() {
		return tierAkwReadTime;
	}

	/**
	 * @param tierAkwReadTime the tierAkwReadTime to set
	 */
	public void setTierAkwReadTime(String tierAkwReadTime) {
		this.tierAkwReadTime = tierAkwReadTime;
	}

	/**
	 * @return the tierApfRead
	 */
	public String getTierApfRead() {
		return tierApfRead;
	}

	/**
	 * @param tierApfRead the tierApfRead to set
	 */
	public void setTierApfRead(String tierApfRead) {
		this.tierApfRead = tierApfRead;
	}

	/**
	 * @return the tierApfReadDate
	 */
	public String getTierApfReadDate() {
		return tierApfReadDate;
	}

	/**
	 * @param tierApfReadDate the tierApfReadDate to set
	 */
	public void setTierApfReadDate(String tierApfReadDate) {
		this.tierApfReadDate = tierApfReadDate;
	}

	/**
	 * @return the tierApfReadTime
	 */
	public String getTierApfReadTime() {
		return tierApfReadTime;
	}

	/**
	 * @param tierApfReadTime the tierApfReadTime to set
	 */
	public void setTierApfReadTime(String tierApfReadTime) {
		this.tierApfReadTime = tierApfReadTime;
	}

	/**
	 * @return the tierBkwhRead
	 */
	public String getTierBkwhRead() {
		return tierBkwhRead;
	}

	/**
	 * @param tierBkwhRead the tierBkwhRead to set
	 */
	public void setTierBkwhRead(String tierBkwhRead) {
		this.tierBkwhRead = tierBkwhRead;
	}

	/**
	 * @return the tierBkwhReadDate
	 */
	public String getTierBkwhReadDate() {
		return tierBkwhReadDate;
	}

	/**
	 * @param tierBkwhReadDate the tierBkwhReadDate to set
	 */
	public void setTierBkwhReadDate(String tierBkwhReadDate) {
		this.tierBkwhReadDate = tierBkwhReadDate;
	}

	/**
	 * @return the tierBkwhReadTime
	 */
	public String getTierBkwhReadTime() {
		return tierBkwhReadTime;
	}

	/**
	 * @param tierBkwhReadTime the tierBkwhReadTime to set
	 */
	public void setTierBkwhReadTime(String tierBkwhReadTime) {
		this.tierBkwhReadTime = tierBkwhReadTime;
	}

	/**
	 * @return the tierBkwRead
	 */
	public String getTierBkwRead() {
		return tierBkwRead;
	}

	/**
	 * @param tierBkwRead the tierBkwRead to set
	 */
	public void setTierBkwRead(String tierBkwRead) {
		this.tierBkwRead = tierBkwRead;
	}

	/**
	 * @return the tierBkwReadDate
	 */
	public String getTierBkwReadDate() {
		return tierBkwReadDate;
	}

	/**
	 * @param tierBkwReadDate the tierBkwReadDate to set
	 */
	public void setTierBkwReadDate(String tierBkwReadDate) {
		this.tierBkwReadDate = tierBkwReadDate;
	}

	/**
	 * @return the tierBkwReadTime
	 */
	public String getTierBkwReadTime() {
		return tierBkwReadTime;
	}

	/**
	 * @param tierBkwReadTime the tierBkwReadTime to set
	 */
	public void setTierBkwReadTime(String tierBkwReadTime) {
		this.tierBkwReadTime = tierBkwReadTime;
	}

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	/*
	 * @Override public String toString() { return "MeterDetail [" + (getFlexNetId()
	 * != null ? "getFlexNetId()=" + getFlexNetId() + ", " : "") + (getMeterId() !=
	 * null ? "getMeterId()=" + getMeterId() + ", " : "") + (getServiceType() !=
	 * null ? "getServiceType()=" + getServiceType() + ", " : "") + (getDeviceType()
	 * != null ? "getDeviceType()=" + getDeviceType() + ", " : "") +
	 * (getSampleRate() != null ? "getSampleRate()=" + getSampleRate() + ", " : "")
	 * + (getCustomerId() != null ? "getCustomerId()=" + getCustomerId() : "") +
	 * "]"; }
	 */}

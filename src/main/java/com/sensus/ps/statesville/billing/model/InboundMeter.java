package com.sensus.ps.statesville.billing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "InboundMeter")
public class InboundMeter {

	/**
	 * Logs messages from Meter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(InboundMeter.class);

	/**
	 * The object-identifier (OID) for this persistable domain object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inboundMeterIDGenerator")
	@SequenceGenerator(name = "inboundMeterIDGenerator", sequenceName = "INBOUNDMETERIDSEQ", allocationSize = 1)
	@Column(name = "ID", updatable = false, nullable = false)
	private Long id = null;

	/**
	 * The version for this persistable domain object. Can be considered the number
	 * of times this given instance has been written to the data-store.
	 */
	@Version
	private long version = 0;

	// SA fields

	@Column(name = "CUSTOMBILLINGCYCLE", length = 10)
	String customBillingCycle;

	@Column(name = "ACCOUNTBILLINGCYCLE", length = 5)
	String accountBillingCycle;

	@Column(name = "ACCOUNTID", length = 15)
	String accountId;

	@Column(name = "ACCOUNTSTATUS", length = 1)
	String accountStatus;

	@Column(name = "CUSTOMSERVICECODE", length = 3)
	String customServiceCode;

	@Column(name = "CUSTOMSLOT", length = 2)
	String customSlot;

	@Column(name = "CUSTOMERNAME", length = 50)
	String customerName;

	@Column(name = "ASSETADDRESS", length = 50)
	String assetAddress;

	@Column(name = "ASSETCITY", length = 20)
	String assetCity;

	@Column(name = "ASSETSTATE", length = 2)
	String assetState;

	@Column(name = "ASSETZIP", length = 5)
	String assetZip;

	@Column(name = "SECURITYTOKEN", length = 5)
	String securityToken;

	@Column(name = "NUMBEROFDIALS", length = 2)
	String numberOfDials;

	@Column(name = "METERID2", length = 20)
	String meterId2;

	@Column(name = "METERID", length = 15)
	String meterId;

	@Column(name = "ERTID", length = 5)
	String ertId;

	@Column(name = "LASTKNOWNREAD", length = 6)
	String lastKnownRead;

	@Column(name = "COMMODITY", length = 1)
	String commodity;

	@Column(name = "METERMANUFACTURER", length = 2)
	String meterManufacturer;

	@Column(name = "ACCOUNTSERVICETYPE", length = 40)
	String accountServiceType;

	@Column(name = "ACCOUNTRATECODE", length = 20)
	String accountRateCode;

	@Column(name = "DISPLAYMULTIPLIER", length = 10)
	String displayMultiplier;

	@Column(name = "CUSTOMMETERSIZE", length = 10)
	String customMeterSize;

	@Column(name = "CUSTOMINCODELATITUDE", length = 25)
	String customIncodeLatitude;

	@Column(name = "CUSTOMINCODELONGITUDE", length = 25)
	String customIncodeLongitude;

	@Column(name = "CUSTOMDEVICESTATUS", length = 1)
	String customDeviceStatus;

	@Column(name = "LASTBILLEDDATE", length = 15)
	String lastBilledDate;

	@Column(name = "MINIMUMUSAGETHRESHOLD", length = 10)
	String minimumUsageThreshold;

	@Column(name = "MAXIMUMUSAGETHRESHOLD", length = 10)
	String maximumUsageThreshold;

	@Column(name = "LOCATIONNUMBER", length = 10)
	String customLocationNumber;

	@Column(name = "LASTDEMANDREAD", length = 15)
	String customLastDemandRead;

	@Column(name = "LASTKVARREAD", length = 15)
	String customLastKvarRead;

	@Column(name = "LASTGENERATEDREAD", length = 15)
	String customLastGeneratedRead;

	// new fields for incode
	@Column(name = "OFFPEAKSLOT", length = 2)
	String offpeakSlot;

	@Column(name = "OFFPEAKSERVICECODE", length = 3)
	String offpeakServiceCode;

	@Column(name = "OFFPEAKMETERID", length = 20)
	String offpeakMeterId;

	public InboundMeter() {
		super();
	}

	/**
	 * Constructs a {@code Meter} from all of its values at once.
	 *
	 * @param offpeakSlot
	 * @param moffpeakServiceCode
	 * @param offpeakMeterId
	 */
	public InboundMeter(final String offpeakSlot, final String offpeakServiceCode, final String offpeakMeterId) {
		super();

		setOffpeakSlot(offpeakSlot);
		setOffpeakServiceCode(offpeakServiceCode);
		setOffpeakMeterId(offpeakMeterId);
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the customBillingCycle
	 */
	public String getCustomBillingCycle() {
		return customBillingCycle;
	}

	/**
	 * @param customBillingCycle the customBillingCycle to set
	 */
	public void setCustomBillingCycle(String customBillingCycle) {
		this.customBillingCycle = customBillingCycle;
	}

	/**
	 * @return the accountBillingCycle
	 */
	public String getAccountBillingCycle() {
		return accountBillingCycle;
	}

	/**
	 * @param accountBillingCycle the accountBillingCycle to set
	 */
	public void setAccountBillingCycle(String accountBillingCycle) {
		this.accountBillingCycle = accountBillingCycle;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus() {
		return accountStatus;
	}

	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	/**
	 * @return the customServiceCode
	 */
	public String getCustomServiceCode() {
		return customServiceCode;
	}

	/**
	 * @param customServiceCode the customServiceCode to set
	 */
	public void setCustomServiceCode(String customServiceCode) {
		this.customServiceCode = customServiceCode;
	}

	/**
	 * @return the customSlot
	 */
	public String getCustomSlot() {
		return customSlot;
	}

	/**
	 * @param customSlot the customSlot to set
	 */
	public void setCustomSlot(String customSlot) {
		this.customSlot = customSlot;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the assetAddress
	 */
	public String getAssetAddress() {
		return assetAddress;
	}

	/**
	 * @param assetAddress the assetAddress to set
	 */
	public void setAssetAddress(String assetAddress) {
		this.assetAddress = assetAddress;
	}

	/**
	 * @return the assetCity
	 */
	public String getAssetCity() {
		return assetCity;
	}

	/**
	 * @param assetCity the assetCity to set
	 */
	public void setAssetCity(String assetCity) {
		this.assetCity = assetCity;
	}

	/**
	 * @return the assetState
	 */
	public String getAssetState() {
		return assetState;
	}

	/**
	 * @param assetState the assetState to set
	 */
	public void setAssetState(String assetState) {
		this.assetState = assetState;
	}

	/**
	 * @return the assetZip
	 */
	public String getAssetZip() {
		return assetZip;
	}

	/**
	 * @param assetZip the assetZip to set
	 */
	public void setAssetZip(String assetZip) {
		this.assetZip = assetZip;
	}

	/**
	 * @return the securityToken
	 */
	public String getSecurityToken() {
		return securityToken;
	}

	/**
	 * @param securityToken the securityToken to set
	 */
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	/**
	 * @return the numberOfDials
	 */
	public String getNumberOfDials() {
		return numberOfDials;
	}

	/**
	 * @param numberOfDials the numberOfDials to set
	 */
	public void setNumberOfDials(String numberOfDials) {
		this.numberOfDials = numberOfDials;
	}

	/**
	 * @return the meterId2
	 */
	public String getMeterId2() {
		return meterId2;
	}

	/**
	 * @param meterId2 the meterId2 to set
	 */
	public void setMeterId2(String meterId2) {
		this.meterId2 = meterId2;
	}

	/**
	 * @return the meterId
	 */
	public String getMeterId() {
		return meterId;
	}

	/**
	 * @param meterId the meterId to set
	 */
	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	/**
	 * @return the ertId
	 */
	public String getErtId() {
		return ertId;
	}

	/**
	 * @param ertId the ertId to set
	 */
	public void setErtId(String ertId) {
		this.ertId = ertId;
	}

	/**
	 * @return the lastKnownRead
	 */
	public String getLastKnownRead() {
		return lastKnownRead;
	}

	/**
	 * @param lastKnownRead the lastKnownRead to set
	 */
	public void setLastKnownRead(String lastKnownRead) {
		this.lastKnownRead = lastKnownRead;
	}

	/**
	 * @return the commodity
	 */
	public String getCommodity() {
		return commodity;
	}

	/**
	 * @param commodity the commodity to set
	 */
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	/**
	 * @return the meterManufacturer
	 */
	public String getMeterManufacturer() {
		return meterManufacturer;
	}

	/**
	 * @param meterManufacturer the meterManufacturer to set
	 */
	public void setMeterManufacturer(String meterManufacturer) {
		this.meterManufacturer = meterManufacturer;
	}

	/**
	 * @return the accountServiceType
	 */
	public String getAccountServiceType() {
		return accountServiceType;
	}

	/**
	 * @param accountServiceType the accountServiceType to set
	 */
	public void setAccountServiceType(String accountServiceType) {
		this.accountServiceType = accountServiceType;
	}

	/**
	 * @return the accountRateCode
	 */
	public String getAccountRateCode() {
		return accountRateCode;
	}

	/**
	 * @param accountRateCode the accountRateCode to set
	 */
	public void setAccountRateCode(String accountRateCode) {
		this.accountRateCode = accountRateCode;
	}

	/**
	 * @return the displayMultiplier
	 */
	public String getDisplayMultiplier() {
		return displayMultiplier;
	}

	/**
	 * @param displayMultiplier the displayMultiplier to set
	 */
	public void setDisplayMultiplier(String displayMultiplier) {
		this.displayMultiplier = displayMultiplier;
	}

	/**
	 * @return the customMeterSize
	 */
	public String getCustomMeterSize() {
		return customMeterSize;
	}

	/**
	 * @param customMeterSize the customMeterSize to set
	 */
	public void setCustomMeterSize(String customMeterSize) {
		this.customMeterSize = customMeterSize;
	}

	/**
	 * @return the customIncodeLatitude
	 */
	public String getCustomIncodeLatitude() {
		return customIncodeLatitude;
	}

	/**
	 * @param customIncodeLatitude the customIncodeLatitude to set
	 */
	public void setCustomIncodeLatitude(String customIncodeLatitude) {
		this.customIncodeLatitude = customIncodeLatitude;
	}

	/**
	 * @return the customIncodeLongitude
	 */
	public String getCustomIncodeLongitude() {
		return customIncodeLongitude;
	}

	/**
	 * @param customIncodeLongitude the customIncodeLongitude to set
	 */
	public void setCustomIncodeLongitude(String customIncodeLongitude) {
		this.customIncodeLongitude = customIncodeLongitude;
	}

	/**
	 * @return the customDeviceStatus
	 */
	public String getCustomDeviceStatus() {
		return customDeviceStatus;
	}

	/**
	 * @param customDeviceStatus the customDeviceStatus to set
	 */
	public void setCustomDeviceStatus(String customDeviceStatus) {
		this.customDeviceStatus = customDeviceStatus;
	}

	/**
	 * @return the lastBilledDate
	 */
	public String getLastBilledDate() {
		return lastBilledDate;
	}

	/**
	 * @param lastBilledDate the lastBilledDate to set
	 */
	public void setLastBilledDate(String lastBilledDate) {
		this.lastBilledDate = lastBilledDate;
	}

	/**
	 * @return the minimumUsageThreshold
	 */
	public String getMinimumUsageThreshold() {
		return minimumUsageThreshold;
	}

	/**
	 * @param minimumUsageThreshold the minimumUsageThreshold to set
	 */
	public void setMinimumUsageThreshold(String minimumUsageThreshold) {
		this.minimumUsageThreshold = minimumUsageThreshold;
	}

	/**
	 * @return the maximumUsageThreshold
	 */
	public String getMaximumUsageThreshold() {
		return maximumUsageThreshold;
	}

	/**
	 * @param maximumUsageThreshold the maximumUsageThreshold to set
	 */
	public void setMaximumUsageThreshold(String maximumUsageThreshold) {
		this.maximumUsageThreshold = maximumUsageThreshold;
	}

	/**
	 * @return the customLocationNumber
	 */
	public String getCustomLocationNumber() {
		return customLocationNumber;
	}

	/**
	 * @param customLocationNumber the customLocationNumber to set
	 */
	public void setCustomLocationNumber(String customLocationNumber) {
		this.customLocationNumber = customLocationNumber;
	}

	/**
	 * @return the customLastDemandRead
	 */
	public String getCustomLastDemandRead() {
		return customLastDemandRead;
	}

	/**
	 * @param customLastDemandRead the customLastDemandRead to set
	 */
	public void setCustomLastDemandRead(String customLastDemandRead) {
		this.customLastDemandRead = customLastDemandRead;
	}

	/**
	 * @return the customLastKvarRead
	 */
	public String getCustomLastKvarRead() {
		return customLastKvarRead;
	}

	/**
	 * @param customLastKvarRead the customLastKvarRead to set
	 */
	public void setCustomLastKvarRead(String customLastKvarRead) {
		this.customLastKvarRead = customLastKvarRead;
	}

	/**
	 * @return the customLastGeneratedRead
	 */
	public String getCustomLastGeneratedRead() {
		return customLastGeneratedRead;
	}

	/**
	 * @param customLastGeneratedRead the customLastGeneratedRead to set
	 */
	public void setCustomLastGeneratedRead(String customLastGeneratedRead) {
		this.customLastGeneratedRead = customLastGeneratedRead;
	}

	/**
	 * @return the offpeakSlot
	 */
	public String getOffpeakSlot() {
		return offpeakSlot;
	}

	/**
	 * @param offpeakSlot the offpeakSlot to set
	 */
	public void setOffpeakSlot(String offpeakSlot) {
		this.offpeakSlot = offpeakSlot;
	}

	/**
	 * @return the offpeakServiceCode
	 */
	public String getOffpeakServiceCode() {
		return offpeakServiceCode;
	}

	/**
	 * @param offpeakServiceCode the offpeakServiceCode to set
	 */
	public void setOffpeakServiceCode(String offpeakServiceCode) {
		this.offpeakServiceCode = offpeakServiceCode;
	}

	/**
	 * @return the offpeakMeterId
	 */
	public String getOffpeakMeterId() {
		return offpeakMeterId;
	}

	/**
	 * @param offpeakMeterId the offpeakMeterId to set
	 */
	public void setOffpeakMeterId(String offpeakMeterId) {
		this.offpeakMeterId = offpeakMeterId;
	}

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	@Override
	public String toString() {
		return "Meter [id=" + id + ", customServiceCode=" + customServiceCode + ", customSlot=" + customSlot
				+ ", customerName=" + customerName + ", meterId2=" + meterId2 + ", meterId=" + meterId
				+ ", offpeakSlot=" + offpeakSlot + ", offpeakServiceCode=" + offpeakServiceCode + ", offpeakMeterId="
				+ offpeakMeterId + "]";
	}

	public String getOutputString() {
		return customBillingCycle + "," + accountBillingCycle + "," + accountId + "," + accountStatus + ","
				+ customServiceCode + "," + customSlot + "," + customerName + "," + assetAddress + "," + assetCity + ","
				+ assetState + "," + assetZip + "," + securityToken + "," + numberOfDials + "," + meterId2 + ","
				+ meterId + "," + ertId + "," + lastKnownRead + "," + commodity + "," + meterManufacturer + ","
				+ accountServiceType + "," + accountRateCode + "," + displayMultiplier + "," + customMeterSize + ","
				+ customIncodeLatitude + "," + customIncodeLongitude + "," + customDeviceStatus + "," + lastBilledDate
				+ "," + minimumUsageThreshold + "," + maximumUsageThreshold + "," + customLocationNumber + ","
				+ customLastDemandRead + "," + customLastKvarRead + "," + customLastGeneratedRead + "," + offpeakSlot
				+ "," + offpeakServiceCode + "," + offpeakMeterId;
	}
}

package com.sensus.ps.statesville.billing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;

/**
 * A {@code Repository} that provides database operations on {@code Meter}s.
 */
@Repository
public interface OutboundInputMeterRepository 
extends JpaRepository<OutboundInputMeter, Long> {
	/**
	 * Saves/updates/persists the passed in {@code Meter}.
	 */
	@SuppressWarnings("unchecked")
	public OutboundInputMeter save(final OutboundInputMeter entity);

	/**
	 * Returns the {@code Meter} that has the provided id/primary-key.
	 */
	public OutboundInputMeter getOne(final Long id);

	/**
	 * Returns a list of meters meters where the meter identifier is the passed in
	 * value.
	 *
	 * @param identifier The sought for meter id.
	 *
	 * @return List of found {@code Meter}s, empty if there are not matches.
	 */
	@Query("from OutboundInputMeter m where m.accountId=?1")
	public List<OutboundInputMeter> findByMeterId(final String identifier);

}

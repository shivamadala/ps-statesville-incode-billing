package com.sensus.ps.statesville.billing.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class is for testing the Jobs during development and is usually not
 * available since Spring is instructed to not start web server by default. ` To
 * use this class, set the following application properties as shown:
 * spring.main.web-application-type=servlet debug=true (primarily for the
 * embedded servlet container's log messages)
 *
 * Open a browser and navigate the the URLs defined on the methods in this
 * controller to execute specific Job/actions.
 */
@RestController
@RequestMapping("/statesville")
public class JobController {
	@Autowired
	private JobLauncher jobLauncher = null;

	@Autowired
	@Qualifier("job-inbound-load-transform-report")
	private Job jobInboundLoadMeter = null;

	@Autowired
	@Qualifier("job-outbound-load-transform-report")
	private Job jobOutboundLoadMeter = null;

	// @Autowired
	// @Qualifier("job-report-meter")
	private Job jobReportMeter = null;

	@GetMapping(path = "/loadInbound")
	public BatchStatus loadInbound() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		final Map<String, JobParameter> map = new HashMap<>();

		map.put("time", new JobParameter(System.currentTimeMillis()));

		final JobParameters parameters = new JobParameters(map);

		final JobExecution jobExecution = jobLauncher.run(jobInboundLoadMeter, parameters);

		return jobExecution.getStatus();
	}

	@GetMapping(path = "/loadOutbound")
	public BatchStatus loadOutbound() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		final Map<String, JobParameter> map = new HashMap<>();

		map.put("time", new JobParameter(System.currentTimeMillis()));

		final JobParameters parameters = new JobParameters(map);

		final JobExecution jobExecution = jobLauncher.run(jobOutboundLoadMeter, parameters);

		return jobExecution.getStatus();
	}

	@GetMapping(path = "/report")
	public BatchStatus reportMeter() throws JobExecutionAlreadyRunningException, JobRestartException,
			JobInstanceAlreadyCompleteException, JobParametersInvalidException {
		final Map<String, JobParameter> map = new HashMap<>();

		map.put("time", new JobParameter(System.currentTimeMillis()));

		final JobParameters parameters = new JobParameters(map);

		final JobExecution jobExecution = jobLauncher.run(jobReportMeter, parameters);

		return jobExecution.getStatus();
	}

}

package com.sensus.ps.statesville.billing.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;

import org.springframework.batch.item.file.mapping.DefaultLineMapper;

import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.core.io.FileSystemResource;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;

public class MeterOutboundFileReader extends FlatFileItemReader<OutboundInputMeter> {
	/**
	 * Logs messages from MeterOutboundFileReader methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterOutboundFileReader.class);

	/**
	 * No-argument constructor.
	 */
	public MeterOutboundFileReader() {
		super();
		// Skip/ignore blank lines.
		setRecordSeparatorPolicy(new MeterRecordSeparatorPolicy());

		// Skip/ignore comment line (those that begin with these Strings).
		//setComments(new String[] { "#", "//", "/*", ";" });

		// Do not raise an Exception if there is no input file.
		setStrict(false);

		setLineMapper(getMeterLineMapper());
	}

	/**
	 * Constructor that takes an input file.
	 */
	public MeterOutboundFileReader(final String inFilePath) {
		this();

		setResource(new FileSystemResource(inFilePath));
	}

	/**
	 * Public setter for the input resource.
	 *
	 * @param inFilePath The path to the input CSV file.
	 *
	 * @return this {@code MeterOutboundFileReader}.
	 */
	public MeterOutboundFileReader setInputFile(final String inFilePath) {
		setResource(new FileSystemResource(inFilePath));

		return this;
	}

	private LineMapper<OutboundInputMeter> getMeterLineMapper() {
		final DefaultLineMapper<OutboundInputMeter> mapper = new DefaultLineMapper<>();
		
		FixedLengthTokenizer tokenizer = new FixedLengthTokenizer ();
		tokenizer.setNames("accountId","blank1","customServiceCode","customSlot","offpeakSlot","offpeakServiceCode","blank2","blank3",
				"kwhRead","kwhReadDate","kwhReadTime","customIncodeLatitude","customIncodeLongitude","kwRead","kwReadDate","kwReadTime",
				"tierAkwhRead","tierAkwhReadDate","tierAkwhReadTime","tierAkwRead","tierAkwReadDate","tierAkwReadTime",
				"tierApfRead","tierApfReadDate","tierApfReadTime","tierBkwhRead","tierBkwhReadDate","tierBkwhReadTime",
				"tierBkwRead","tierBkwReadDate","tierBkwReadTime");

      tokenizer.setColumns(
    		// accountId
    		  new Range(1,12), 
    		  new Range(13,14), // blank1
    		// customServiceCode
    		  new Range(15,17),    
    		// customSlot
    		  new Range(18,19),   
    		// offpeakSlot
    		  new Range(20,21),   
    		// offpeakServiceCode
    		  new Range(22,24),   
    		  new Range(25,25),   // blank2
    		  new Range(26,26),   // blank3
    		// kwhRead
    		  new Range(27,35),   
    		  new Range(36,41),
    		  new Range(42,47),   
    		// customIncodeLatitude
    		  new Range(48,58),   
    		// customIncodeLongitude
    		  new Range(59,69),   
    		// kwRead
    		  new Range(70,78),   
    		  new Range(79,84),   
    		  new Range(85,90),   
    		// tierAkwhRead
    		  new Range(91,99),   
    		  new Range(100,105),   
    		  new Range(106,111),   
    		// tierAkwRead
    		  new Range(112,120),   
    		  new Range(121,126),   
    		  new Range(127,132),   
    		// tierApfRead
    		  new Range(133,141),   
    		  new Range(142,147),    
    		  new Range(148,153),    
    		// tierBkwhRead
    		  new Range(154,162),   
    		  new Range(163,168),    
    		  new Range(169,174),
    		// tierBkwRead
    		  new Range(175,183),   
    		  new Range(184,189),    
    		  new Range(190,195));
		
//        tokenizer.setColumns(new Range(1,12),
//                             new Range(13,14),
//                             new Range(15,17),
//                             new Range(18,19),
//                             new Range(20,20),
//                             new Range(21,21),
//                             new Range(22,30),
//                             new Range(31,36),
//                             new Range(37,42),
//                             new Range(43,53),
//                             new Range(54,64),
//                             new Range(65,73),
//                             new Range(74,79),
//                             new Range(80,85),
//                             new Range(86,94),
//                             new Range(95,100),
//                             new Range(101,106));
		tokenizer.setStrict(false);
		
//		final DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
//		tokenizer.setDelimiter(",");
//		tokenizer.setStrict(false);
//		tokenizer.setNames("accountId","blank1","customServiceCode","customSlot","offpeakServiceCode","offpeakSlot","blank2","blank3",
//				"kwhRead","kwhReadDate","kwhReadTime","customIncodeLatitude","customIncodeLongitude","kwRead","kwReadDate","kwReadTime",
//				"tierAkwhRead","tierAkwhReadDate","tierAkwhReadTime","tierAkwRead","tierAkwReadDate","tierAkwReadTime",
//				"tierApfRead","tierApfReadDate","tierApfReadTime","tierBkwhRead","tierBkwhReadDate","tierBkwhReadTime",
//				"tierBkwRead","tierBkwReadDate","tierBkwReadTime");

		mapper.setLineTokenizer(tokenizer);
		mapper.setFieldSetMapper(new MeterOutboundInputFieldSetMapper());

		return mapper;
	}

	@Override
	public OutboundInputMeter read() throws Exception, UnexpectedInputException, ParseException {
		final OutboundInputMeter meter = super.read();

		if (meter != null) {
			logger.debug("Read Account: {}", meter);
		}

		return meter;
	}
}

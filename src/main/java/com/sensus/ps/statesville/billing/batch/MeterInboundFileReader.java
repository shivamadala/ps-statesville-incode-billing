package com.sensus.ps.statesville.billing.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;

import org.springframework.batch.item.file.mapping.DefaultLineMapper;

import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;

import org.springframework.core.io.FileSystemResource;

import com.sensus.ps.statesville.billing.model.InboundMeter;

public class MeterInboundFileReader extends FlatFileItemReader<InboundMeter> {
	/**
	 * Logs messages from MeterFileReader methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundFileReader.class);

	/**
	 * No-argument constructor.
	 */
	public MeterInboundFileReader() {
		super();
		// Skip/ignore blank lines.
		setRecordSeparatorPolicy(new MeterRecordSeparatorPolicy());

		// Skip/ignore comment line (those that begin with these Strings).
		setComments(new String[] { "#", "//", "/*", ";" });

		// Do not raise an Exception if there is no input file.
		setStrict(false);

		setLineMapper(getMeterLineMapper());
	}

	/**
	 * Constructor that takes an input file.
	 */
	public MeterInboundFileReader(final String inFilePath) {
		this();

		setResource(new FileSystemResource(inFilePath));
	}

	/**
	 * Public setter for the input resource.
	 *
	 * @param inFilePath The path to the input CSV file.
	 *
	 * @return this {@code MeterFileReader}.
	 */
	public MeterInboundFileReader setInputFile(final String inFilePath) {
		setResource(new FileSystemResource(inFilePath));

		return this;
	}

	private LineMapper<InboundMeter> getMeterLineMapper() {
		final DefaultLineMapper<InboundMeter> mapper = new DefaultLineMapper<>();
		final DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();

		tokenizer.setDelimiter(",");
		tokenizer.setStrict(false);
		tokenizer.setNames("customBillingCycle", "accountBillingCycle", "accountId", "accountStatus",
				"customServiceCode", "customSlot", "customerName", "assetAddress", "assetCity", "assetState",
				"assetZip", "securityToken", "numberOfDials", "meterId2", "meterId", "ertId", "lastKnownRead",
				"commodity", "meterManufacturer", "accountServiceType", "accountRateCode", "displayMultiplier",
				"customMeterSize", "customIncodeLatitude", "customIncodeLongitude", "customDeviceStatus",
				"lastBilledDate", "minimumUsageThreshold", "maximumUsageThreshold", "customLocationNumber",
				"customLastDemandRead", "customLastKvarRead", "customLastGeneratedRead", "offpeakSlot",
				"offpeakServiceCode", "offpeakMeterId");

		mapper.setLineTokenizer(tokenizer);
		mapper.setFieldSetMapper(new MeterInboundFieldSetMapper());

		return mapper;
	}

	@Override
	public InboundMeter read() throws Exception, UnexpectedInputException, ParseException {
		final InboundMeter meter = super.read();

		if (meter != null) {
			logger.debug("Read meter: {}", meter);
		}

		return meter;
	}
}

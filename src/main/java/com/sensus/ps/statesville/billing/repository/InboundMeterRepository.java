package com.sensus.ps.statesville.billing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sensus.ps.statesville.billing.model.InboundMeter;

/**
 * A {@code Repository} that provides database operations on {@code Meter}s.
 */
@Repository
public interface InboundMeterRepository 
extends JpaRepository<InboundMeter, Long> {
	/**
	 * Saves/updates/persists the passed in {@code Meter}.
	 */
	@SuppressWarnings("unchecked")
	public InboundMeter save(final InboundMeter entity);

	/**
	 * Returns the {@code Meter} that has the provided id/primary-key.
	 */
	public InboundMeter getOne(final Long id);

	/**
	 * Returns a list of meters meters where the meter identifier is the passed in
	 * value.
	 *
	 * @param identifier The sought for meter id.
	 *
	 * @return List of found {@code Meter}s, empty if there are not matches.
	 */
	@Query("from InboundMeter m where m.meterId=?1")
	public List<InboundMeter> findByMeterId(final String identifier);

	/**
	 * Returns a list of meters meters where the offpeak meter identifier is the
	 * passed in value.
	 *
	 * @param identifier The sought for offpeak meter id.
	 *
	 * @return List of found {@code Meter}s, empty if there are not matches.
	 */
	@Query("from InboundMeter m where m.meterId2=?1")
	public List<InboundMeter> findByOffpeakMeterId(final String identifier);

}

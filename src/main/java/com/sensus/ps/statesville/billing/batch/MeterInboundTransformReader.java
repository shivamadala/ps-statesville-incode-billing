package com.sensus.ps.statesville.billing.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.database.JpaPagingItemReader;

import com.sensus.ps.statesville.billing.model.InboundMeter;

public class MeterInboundTransformReader 
extends JpaPagingItemReader<InboundMeter> {
	/**
	 * Logs messages from MeterTransformReader methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundTransformReader.class);

    /**
     * Query that selects meters for encryption processing.
     */
    private static final String METER_QUERY =
        "SELECT "
            + "m "
        + "FROM "
            + "InboundMeter m ";

	/**
	 * No-argument constructor. The default reporting window is is 30-days.
	 */
	public MeterInboundTransformReader() {
		super();
        setQueryString(METER_QUERY);

	}

	@Override
	public InboundMeter read() throws Exception, UnexpectedInputException, ParseException {
		// Currently no additional functionality is required. This is behaving
		// as a "pass-through" reader would. Method implemented for debugging.
		final InboundMeter meter = super.read();

		if (meter != null) {
			MeterInboundTransformReader.logger.debug("Read meter: {}", meter);
		}

		return meter;
	}

}

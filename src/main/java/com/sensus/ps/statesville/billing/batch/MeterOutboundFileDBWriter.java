package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;
import com.sensus.ps.statesville.billing.repository.OutboundInputMeterRepository;

public class MeterOutboundFileDBWriter implements ItemWriter<OutboundInputMeter> {

	/**
	 * Logs messages from MeterOutboundFileDBWriter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterOutboundFileDBWriter.class);

	/**
	 * A {@code OutboundInputMeterRepository} for performing
	 * {@code OutboundInputMeter} database operations.
	 */
	private OutboundInputMeterRepository outboundInputMeterRepository = null;

	/**
	 * No-argument constructor.
	 */
	public MeterOutboundFileDBWriter() {
		super();
	}

	/**
	 * Returns the {@code OutboundInputMeterRepository} that is used to perform
	 * database operations on {@code Account} instances.
	 *
	 * @return A {@code OutboundInputMeterRepository} for performing
	 *         {@code OutboundInputMeter} operations.
	 */
	public OutboundInputMeterRepository getOutboundInputMeterRepository() {
		if (this.outboundInputMeterRepository == null) {
			final String message = "Invalid/null OutboundInputMeterRepository";

			MeterOutboundFileDBWriter.logger.error(message);

			throw new IllegalStateException(message);
		}

		return this.outboundInputMeterRepository;
	}

	/**
	 * Provide a {@code OutboundInputMeterRepository} for this instance to use.
	 *
	 * @param value {@code OutboundInputMeterRepository} for performing
	 *              {@code Account} operations.
	 *
	 * @return this {@code MeterFileDBWriter} instance.
	 */
	@Autowired
	private MeterOutboundFileDBWriter setOutboundInputMeterRepository(final OutboundInputMeterRepository value) {
		if (value == null) {
			final String message = "Invalid/null OutboundInputMeterRepository";

			logger.error(message);

			throw new IllegalArgumentException(message);
		}

		this.outboundInputMeterRepository = value;

		return this;
	}

	/**
	 * Save all the {@code OutboundInputMeter}s in the passed in {@code List} that
	 * are new (have account identifiers that have not been seen before).
	 *
	 * @param items A list of {@code OutboundInputMeter} instances.
	 *
	 * @return this {@code MeterOutboundFileDBWriter} instance.
	 *
	 * @throws Exception if a persistence operation fails.
	 */
	public MeterOutboundFileDBWriter writeNewMeters(final List<? extends OutboundInputMeter> items) throws Exception {
		// The passed in list is empty (or null).
		if (isEmpty(items)) {
			logger.debug("No Accounts provided");

			return this;
		}

		for (final OutboundInputMeter item : items) {
			if (item == null) {
				continue;
			}

			logger.info("Processing account '{}'", item.getAccountId());

			final Long id = item.getId();

			// This account has already been persisted so skip it.
			if (id != null) {
				logger.info("Account {}/{} already persisted; skipping", item.getAccountId(), item.getId());

				continue;
			}

			final String accountId = item.getAccountId();

			// This Account contains invalid data so skip it.
			if (isBlank(accountId)) {
				logger.warn("Account record has invalid/blank Account Id; skipping");

				continue;
			}

//			// Populate On Peak meter details from Input Meter
//			OutboundMeter onPeakMeter = new OutboundMeter();
//			onPeakMeter.populateOnPeakMeter(item);
//			getOutboundInputMeterRepository().save(onPeakMeter);
//
//			// Populate Off Peak meter details from Input Meter
//			OutboundMeter offPeakMeter = new OutboundMeter();
//			offPeakMeter.populateOffPeakMeter(item);
//			getOutboundInputMeterRepository().save(offPeakMeter);

			getOutboundInputMeterRepository().save(item);
			logger.debug(item.toString());

			logger.info("Saved account '{}'", accountId);
		}

		return this;
	}

	@Override
	public void write(List<? extends OutboundInputMeter> items) throws Exception {
		if (isNotEmpty(items)) {
			writeNewMeters(items);
		}
	}
}

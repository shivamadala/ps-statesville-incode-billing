package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.sensus.ps.statesville.billing.model.InboundMeter;
import com.sensus.ps.statesville.billing.repository.InboundMeterRepository;

public class MeterInboundFileDBWriter implements ItemWriter<InboundMeter> {

	/**
	 * Logs messages from MeterFileDBWriter methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterInboundFileDBWriter.class);

	/**
	 * A {@code Repository} for performing {@code Meter} database operations.
	 */
	private InboundMeterRepository repository = null;

	/**
	 * No-argument constructor.
	 */
	public MeterInboundFileDBWriter() {
		super();
	}

	/**
	 * Returns the {@code MeterRepository} that is used to perform database
	 * operations on {@code Meter} instances.
	 *
	 * @return A {@code Repository} for performing {@code Meter} operations.
	 */
	public InboundMeterRepository getMeterRepository() {
		if (this.repository == null) {
			final String message = "Invalid/null MeterRepository";

			MeterInboundFileDBWriter.logger.error(message);

			throw new IllegalStateException(message);
		}

		return this.repository;
	}

	/**
	 * Provide a {@code MeterRepository} for this instance to use.
	 *
	 * @param value {@code Repository} for performing {@code Meter} operations.
	 *
	 * @return this {@code MeterFileDBWriter} instance.
	 */
	@Autowired
	private MeterInboundFileDBWriter setMeterRepository(final InboundMeterRepository value) {
		if (value == null) {
			final String message = "Invalid/null MeterRepository";

			logger.error(message);

			throw new IllegalArgumentException(message);
		}

		this.repository = value;

		return this;
	}

	/**
	 * Save all the {@code Meter}s in the passed in {@code List} that are new (have
	 * meter identifiers that have not been seen before).
	 *
	 * @param items A list of {@code Meter} instances.
	 *
	 * @return this {@code MeterFileDBWriter} instance.
	 *
	 * @throws Exception if a persistence operation fails.
	 */
	public MeterInboundFileDBWriter writeNewMeters(final List<? extends InboundMeter> items) throws Exception {
		// The passed in list is empty (or null).
		if (isEmpty(items)) {
			logger.debug("No Meters provided");

			return this;
		}

		for (final InboundMeter item : items) {
			if (item == null) {
				continue;
			}

			logger.info("Processing meter '{}'", item.getMeterId());

			final Long id = item.getId();

			// This meter has already been persisted so skip it.
			if (id != null) {
				logger.info("Meter {}/{} already persisted; skipping", item.getMeterId(), item.getId());

				continue;
			}

			final String meterId = item.getMeterId();

			// This Meter contains invalid data so skip it.
			if (isBlank(meterId)) {
				logger.warn("Meter record has invalid/blank Meter Id; skipping");

				continue;
			}

			getMeterRepository().save(item);

			logger.debug(item.toString());

			logger.info("Saved meter '{}'", meterId);
		}

		return this;
	}

	@Override
	public void write(List<? extends InboundMeter> items) throws Exception {
		if (isNotEmpty(items)) {
			writeNewMeters(items);
		}
	}
}

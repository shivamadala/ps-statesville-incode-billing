package com.sensus.ps.statesville.billing.batch;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.sensus.ps.statesville.billing.model.InboundMeter;

public class MeterInboundFieldSetMapper implements FieldSetMapper<InboundMeter> {
	@Override
	public InboundMeter mapFieldSet(final FieldSet fieldSet) throws BindException {
		if ((fieldSet == null) || (fieldSet.getFieldCount() < 1)) {
			return null;
		}

		final String meterId = fieldSet.readString("meterId");

		if (StringUtils.isBlank(meterId)) {
			return null;
		}

		final InboundMeter meter = new InboundMeter();

		meter.setCustomBillingCycle(fieldSet.readString("customBillingCycle"));
		meter.setAccountBillingCycle(fieldSet.readString("accountBillingCycle"));
		meter.setAccountId(fieldSet.readString("accountId"));
		meter.setAccountStatus(fieldSet.readString("accountStatus"));
		meter.setCustomServiceCode(fieldSet.readString("customServiceCode"));
		meter.setCustomSlot(fieldSet.readString("customSlot"));
		meter.setCustomerName(fieldSet.readString("customerName"));
		meter.setAssetAddress(fieldSet.readString("assetAddress"));
		meter.setAssetCity(fieldSet.readString("assetCity"));
		meter.setAssetState(fieldSet.readString("assetState"));
		meter.setAssetZip(fieldSet.readString("assetZip"));
		meter.setSecurityToken(fieldSet.readString("securityToken"));
		meter.setNumberOfDials(fieldSet.readString("numberOfDials"));
		meter.setMeterId2(fieldSet.readString("meterId2"));
		meter.setMeterId(fieldSet.readString("meterId"));
		meter.setErtId(fieldSet.readString("ertId"));
		meter.setLastKnownRead(fieldSet.readString("lastKnownRead"));
		meter.setCommodity(fieldSet.readString("commodity"));
		meter.setMeterManufacturer(fieldSet.readString("meterManufacturer"));
		meter.setAccountServiceType(fieldSet.readString("accountServiceType"));
		meter.setAccountRateCode(fieldSet.readString("accountRateCode"));
		meter.setDisplayMultiplier(fieldSet.readString("displayMultiplier"));
		meter.setCustomMeterSize(fieldSet.readString("customMeterSize"));
		meter.setCustomIncodeLatitude(fieldSet.readString("customIncodeLatitude"));
		meter.setCustomIncodeLongitude(fieldSet.readString("customIncodeLongitude"));
		meter.setCustomDeviceStatus(fieldSet.readString("customDeviceStatus"));
		meter.setLastBilledDate(fieldSet.readString("lastBilledDate"));
		meter.setMinimumUsageThreshold(fieldSet.readString("minimumUsageThreshold"));
		meter.setMaximumUsageThreshold(fieldSet.readString("maximumUsageThreshold"));
		meter.setCustomLocationNumber(fieldSet.readString("customLocationNumber"));
		meter.setCustomLastDemandRead(fieldSet.readString("customLastDemandRead"));
		meter.setCustomLastKvarRead(fieldSet.readString("customLastKvarRead"));
		meter.setCustomLastGeneratedRead(fieldSet.readString("customLastGeneratedRead"));
		meter.setOffpeakSlot(fieldSet.readString("offpeakSlot"));
		meter.setOffpeakServiceCode(fieldSet.readString("offpeakServiceCode"));
		meter.setOffpeakMeterId(fieldSet.readString("offpeakMeterId"));

		return meter;
	}
}

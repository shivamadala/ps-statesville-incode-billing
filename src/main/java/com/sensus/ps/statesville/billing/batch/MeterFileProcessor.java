package com.sensus.ps.statesville.billing.batch;

import org.springframework.batch.item.ItemProcessor;

import com.sensus.ps.statesville.billing.model.InboundMeter;

public class MeterFileProcessor implements ItemProcessor<InboundMeter, InboundMeter> {

	@Override
	public InboundMeter process(final InboundMeter meter) throws Exception {

		return meter;
	}
}

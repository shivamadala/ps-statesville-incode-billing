package com.sensus.ps.statesville.billing.batch;


import java.io.BufferedReader;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.separator.SimpleRecordSeparatorPolicy;


/**
 * Instructs a FlatFileItemReader how to handle blank lines.
 */
public class MeterRecordSeparatorPolicy
extends SimpleRecordSeparatorPolicy {
    /**
     * Signal the end of a record based on the content of the current record.
     * During the course of processing, each time this method returns false,
     * the next line read is appended onto it (building the record).  The input
     * is what you would expect from {@link BufferedReader#readLine()} - i.e.
     * no line separator character at the end. But it might have line separators
     * embedded in it.
     *
     * @param line a String without a newline character at the end.
     *
     * @return true if this line is a complete record.
     */
    @Override
    public boolean isEndOfRecord(final String line) {
        return (line == null)
            || line.trim().isEmpty()
            || super.isEndOfRecord(line);
    }

    /**
     * Give the policy a chance to post-process a complete record, e.g. remove a
     * suffix.
     *
     * @param record the complete record.
     * @return a modified version of the record if desired.
     */
    @Override
    public String postProcess(final String record) {
        final String r = StringUtils.trimToNull(record);

        return (r == null)
            ? null
            : super.postProcess(r);
    }
}

package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import java.io.File;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.http.client.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.JobInstance;

/**
 * A {@code JobExecutionListener} that moves the input files to the archive
 * directory after the associated {@code Job} ends.
 */
public class MoveFilesJobListener implements JobExecutionListener {
	/**
	 * Logs messages from MoveFilesJobListener methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MoveFilesJobListener.class);

	/**
	 * The name/path to the input directory.
	 */
	private String inputDirectoryName = null;

	/**
	 * The name/path to the archive directory.
	 */
	private String archiveDirectoryName = null;

	/**
	 * The file extension to the files.
	 */
	private String fileExtension = null;

	/**
	 * Returns the name/path of the input (source) directory.
	 *
	 * @return the path to the directory where the input files are located.
	 */
	public String getInputDirectory() {
		return this.inputDirectoryName;
	}

	/**
	 * Sets the name/path of the input (source) directory.
	 *
	 * @param value The path to the directory where the input files are located.
	 *
	 * @throws RuntimeException If the provided value is invalid.
	 */
	public void setInputDirectory(final String value) throws RuntimeException {
		final String in = trimToEmpty(value);

		if (isBlank(in)) {
			throw new IllegalArgumentException("Invalid/blank path to input directory");
		}

		this.inputDirectoryName = in;
	}

	/**
	 * Returns the name/path of the archive (destination) directory.
	 *
	 * @return the path to the directory where the input files are archived.
	 */
	public String getArchiveDirectory() {
		return this.archiveDirectoryName;
	}

	/**
	 * Sets the name/path of the target (destination) directory.
	 *
	 * @param value The path to directory where the input files are archived.
	 *
	 * @throws RuntimeException If the provided value is invalid.
	 */
	public void setArchiveDirectory(final String value) throws RuntimeException {
		final String out = trimToEmpty(value);

		if (isBlank(out)) {
			throw new IllegalArgumentException("Invalid/blank path to archive directory");
		}

		this.archiveDirectoryName = out;
	}

	/**
	 * Returns the file extension.
	 * 
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * Sets the file extension.
	 * 
	 * @param fileExtension the fileExtension to set
	 * 
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * Creates a {@code File} using the provided path.
	 *
	 * @param path The file name/path/location.
	 *
	 * @return A {@code File} to the specified location.
	 */
	private File asDirectory(final String path) {
		final File file = new File(trimToEmpty(path));

		if (file.exists() && !file.isDirectory()) {
			final String message = String.format("Not a directory: '%s'", file.getAbsolutePath());

			logger.error(message);

			throw new IllegalArgumentException(message);
		}

		return file;
	}

	/**
	 * Returns a {@code Collection} of the {@code File}s that are located in the
	 * given directory and are regular files that have the extension "*.csv"
	 *
	 * @param sourceDirectory The directory to search.
	 *
	 * @return A collection of {@code File}s.
	 */
	private Collection<File> listOfFiles(final File sourceDirectory) {
		
		logger.debug("Source Directory: {}", sourceDirectory.getAbsolutePath());
		
		final IOFileFilter filter = new IOFileFilter() {
			@Override
			public boolean accept(final File dir, final String name) {
				if (dir == null) {
					return false;
				}

				final String n = trimToEmpty(name);

				if (isBlank(n)) {
					return false;
				}

				return n.endsWith(fileExtension);
			}

			/**
			 * Checks to see if the passed in {@code File} should be accepted by this
			 * filter.
			 */
			@Override
			public boolean accept(final File file) {
				if ((file == null) || !file.isFile() || file.isDirectory()) {
					return false;
				}

				return file.getName().endsWith(fileExtension);
			}
		};

		return FileUtils.listFiles(sourceDirectory, filter, null);
	}

	/**
	 * Move files from the input directory to the archive one.
	 *
	 * @param sourceDirectory  The {@code File} source/input directory.
	 * @param archiveDirectory The {@code File} target/archive directory.
	 */
	public void moveFilesToArchive(final File sourceDirectory, final File archiveDirectory) {
		
		logger.debug("Source Directory: {}", sourceDirectory.getAbsolutePath());
		logger.debug("Archive Directory: {}", archiveDirectory.getAbsolutePath());
		
		final Collection<File> inputFiles = listOfFiles(sourceDirectory);

		// No files to move.
		if (CollectionUtils.isEmpty(inputFiles)) {
			logger.info("The directory '{}' does not contain any input " + "files to archive",
					sourceDirectory.getAbsolutePath());

			return;
		}

		// Move each of the processed input files to the archive directory.
		for (final File inputFile : inputFiles) {
			// Not a regular file, do not attempt to move.
			if ((inputFile == null) || !inputFile.isFile() || inputFile.isDirectory()) {
				continue;
			}

			try {
				FileUtils.moveToDirectory(inputFile, archiveDirectory, true);

				logger.info("Moved '{}' to '{}'", inputFile.getName(), archiveDirectory.getAbsolutePath());

			} catch (final Exception ex) {
				MoveFilesJobListener.logger.warn("Cannot move '{}' to '{}': {}", inputFile.getAbsolutePath(),
						archiveDirectory.getAbsolutePath(), ex.toString());
			}
		}
	}

	/**
	 * Callback before a job executes.
	 */
	@Override
	public void beforeJob(final JobExecution jobExecution) {
		// No-op
	}

	/**
	 * Callback after completion of a job. Called after both both successful and
	 * failed executions. To perform logic on a particular status, use "if
	 * (jobExecution.getStatus() == BatchStatus.X)".
	 */
	@Override
	public void afterJob(final JobExecution jobExecution) {
		final JobInstance jobInstance = jobExecution.getJobInstance();
		final BatchStatus jobStatus = jobExecution.getStatus();

		final String jobName = (jobInstance == null) ? "spring-batch-job"
				: defaultIfBlank(jobInstance.getJobName(), "spring-batch-job");

		logger.debug("JobName {}",jobName);
		
		if (BatchStatus.COMPLETED.equals(jobStatus)) {
			logger.info("Job {} status: {}, moving input files (if any) to archive", jobName, jobStatus);

			logger.info("Input Directory {}, {}", getInputDirectory(), asDirectory(getInputDirectory()));
			logger.info("Archive Directory {}, {}", getArchiveDirectory(), asDirectory(getArchiveDirectory()));
			
			moveFilesToArchive(asDirectory(getInputDirectory()), asDirectory(getArchiveDirectory()));
		} else {
			logger.warn("Job {} status: {}, input files (if any) not moved to archive", jobName, jobStatus);
		}
	}

	/**
	 * No-argument constructor.
	 */
	public MoveFilesJobListener() {
		super();
	}

	/**
	 * Constructor that takes a source path (the directory where the files to move
	 * are initially located) and a destination path (the directory where those
	 * files are to be moved).
	 *
	 * @param source        The path/name/location of the source directory.
	 * @param destination   The path/name/location of the destination directory.
	 * @param fileExtension The file extension of the files to be moved.
	 */
	public MoveFilesJobListener(final String source, final String destination, final String fileExtension) {
		this();

		setInputDirectory(source);
		setArchiveDirectory(destination);
		setFileExtension(fileExtension);
	}
}

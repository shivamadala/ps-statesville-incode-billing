package com.sensus.ps.statesville.billing.batch;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.sensus.ps.statesville.billing.model.OutboundInputMeter;
import com.sensus.ps.statesville.billing.model.OutboundMeter;

public class MeterOutboundTransformProcessor implements ItemProcessor<OutboundInputMeter, List<OutboundMeter>> {

	/**
	 * Logs messages from MeterOutboundTransformProcessor methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(MeterOutboundTransformProcessor.class);

	/**
	 * Returns collection of {@code OutboundMeter} based on the input of
	 * {@code OutboundInputMeter} instances.
	 *
	 * @return A collection of {@code OutboundMeter}.
	 */
	@Override
	public List<OutboundMeter> process(final OutboundInputMeter meter) throws Exception {

		List<OutboundMeter> outboundMeters = new ArrayList<OutboundMeter>();

		OutboundMeter onPeakMeter = new OutboundMeter();
		onPeakMeter.setAccountId(meter.getAccountId());
		onPeakMeter.setServiceCode(meter.getCustomServiceCode());
		onPeakMeter.setServiceSlot(meter.getCustomSlot());
		onPeakMeter.setIncodeLatitude(meter.getIncodeLatitude());
		onPeakMeter.setIncodeLongitude(meter.getIncodeLongitude());
		onPeakMeter.setNonTouKwhRead(meter.getTierAkwhRead());
		onPeakMeter.setNonTouKwhReadDate(meter.getTierAkwhReadDate());
		onPeakMeter.setNonTouKwhReadTime(meter.getTierAkwhReadTime());
		onPeakMeter.setNonTouKwRead(meter.getTierAkwRead());
		onPeakMeter.setNonTouKwReadDate(meter.getTierAkwReadDate());
		onPeakMeter.setNonTouKwReadTime(meter.getTierAkwReadTime());
		onPeakMeter.setTouTier1Read(meter.getTierApfRead());
		onPeakMeter.setTouTier1ReadDate(meter.getTierApfReadDate());
		onPeakMeter.setTouTier1ReadTime(meter.getTierApfReadTime());

		outboundMeters.add(onPeakMeter);

		logger.debug("meter.getOffpeakServiceCode() '{}', size {}", meter.getOffpeakServiceCode(), meter.getOffpeakServiceCode().length());
		
		// if no off peak meter, dont add it to the list
		if (isNotEmpty(meter.getOffpeakServiceCode().trim())) {

			OutboundMeter offPeakMeter = new OutboundMeter();
			offPeakMeter.setAccountId(meter.getAccountId());
			offPeakMeter.setServiceCode(meter.getOffpeakServiceCode());
			offPeakMeter.setServiceSlot(meter.getOffpeakSlot());
			offPeakMeter.setIncodeLatitude(meter.getIncodeLatitude());
			offPeakMeter.setIncodeLongitude(meter.getIncodeLongitude());
			offPeakMeter.setNonTouKwhRead(meter.getTierBkwhRead());
			offPeakMeter.setNonTouKwhReadDate(meter.getTierBkwhReadDate());
			offPeakMeter.setNonTouKwhReadTime(meter.getTierBkwhReadTime());
			offPeakMeter.setNonTouKwRead(meter.getTierBkwRead());
			offPeakMeter.setNonTouKwReadDate(meter.getTierBkwReadDate());
			offPeakMeter.setNonTouKwReadTime(meter.getTierBkwhReadTime());
			offPeakMeter.setTouTier1Read("");
			offPeakMeter.setTouTier1ReadDate("");
			offPeakMeter.setTouTier1ReadTime("");

			outboundMeters.add(offPeakMeter);

		}

		logger.debug("Offpeak Meters found for {}, {}", meter.getAccountId(), outboundMeters.size());

		return outboundMeters;
	}

}

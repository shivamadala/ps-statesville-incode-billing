package com.sensus.ps.statesville.billing;


import static org.apache.commons.collections4.MapUtils.isEmpty;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

import static org.apache.commons.lang3.BooleanUtils.toBooleanObject;

import static org.apache.commons.lang3.StringUtils.equalsAny;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import static org.apache.commons.lang3.math.NumberUtils.toInt;
import static org.apache.commons.lang3.math.NumberUtils.toLong;

import java.text.SimpleDateFormat;

import java.time.ZonedDateTime;

import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Group of utility methods used by this project.
 */
public class Utility {
    /**
     * Log messages from Utility methods.
     */
    private static final Logger logger = LoggerFactory.getLogger(Utility.class);

    /**
     * The separator used between elements when arrays, collections, and maps
     * are converted to a meaningful/displayable textual representation. 
     */
    private static final String DEFAULT_SEPARATOR = ", ";

    /**
     * Used to format/parse date strings.
     */
    private static SimpleDateFormat dateFormatter = null;

    static {
        Utility.dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Utility.dateFormatter.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
    }

    /**
     * Causes the currently executing thread to sleep for the specified number
     * of milliseconds. {@link Thread#sleep(long)}.
     *
     * @param value The length of time to sleep in milliseconds.
     */
    public static void sleep(final long value) {
        if (value < 0) {
            return;
        }

        try {
            Thread.sleep(value);
        } catch (@SuppressWarnings("unused") final InterruptedException ex) {
            // No-op
        }
    }

    /**
     * Causes the currently executing thread to sleep for the specified number
     * of seconds.
     *
     * @param value The length of time to sleep in seconds.
     */
    public static void sleepSeconds(final int value) {
        Utility.sleep(value * 1000L);
    }

    /**
     * Causes the currently executing thread to sleep for the specified number
     * of milliseconds.  If the provided value cannot be interpreted as a
     * numeric value then the thread will sleep for zero milliseconds.
     *
     * @param value The length of time to sleep in milliseconds.
     */
    public static void sleep(final String value) {
        Utility.sleep(toLong(trimToEmpty(value), 0));
    }

    /**
     * Causes the currently executing thread to sleep for the specified number
     * of seconds.  If the provided value cannot be interpreted as a
     * numeric value then the thread will sleep for zero seconds.
     *
     * @param value The length of time to sleep in seconds.
     */
    public static void sleepSeconds(final String value) {
        Utility.sleepSeconds(toInt(trimToEmpty(value), 0));
    }

    /**
     * Returns an {@code Integer} value created from the contents of the given
     * {@code String}.  If the provided value cannot be interpreted as a
     * numeric value then {@code null} is returned.
     *
     * @param value The value to use to build a {@code Double}.
     *
     * @return A {@code Integer} or {@code null} if value cannot be converted.
     */
    public static Integer asInteger(final String value) {
        final String s;

        if (isEmpty(s = trimToEmpty(value))) {
            return null;
        }

        try {
            return Integer.valueOf(s);
        } catch (@SuppressWarnings("unused") final Exception ex) {
            // No-op
        }

        Utility.logger.warn(
            "Unable to convert '{}' to an Integer; returning null",
            value);

        return null;
    }

    /**
     * Returns an {@code int} equivalent of the provided {@code Integer}.
     *
     * @param value An {@code Integer}.
     *
     * @return A {@code int} int value of the passed in Integer if it is not
     *      null, otherwise 0.
     */
    public static int integerValue(final Integer value) {
        return (value == null)
            ? 0
            : value.intValue();
    }

    /**
     * Returns an {@code int} equivalent of the provided {@code String}.
     *
     * @param value A {@code String} holding a textual representation of a value
     *      that can be converted to an {@code int}.
     *
     * @return A {@code int} numeric value if the parameter can be converted
     *      to a numeric value, or 0 otherwise.
     */
    public static int integerValue(final String value) {
        return Utility.integerValue(Utility.asInteger(value));
    }

    /**
     * Returns a {@code long} equivalent of the provided {@code Long}.
     *
     * @param value A {@code Long}.
     *
     * @return A {@code long} long value of the passed in Long if it is not
     *      null, otherwise 0.
     */
    public static long longValue(final Long value) {
        return (value == null)
            ? 0L
            : value.longValue();
    }

    /**
     * Converts a {@code String} to a {@code Boolean}.
     *
     * @param value the {@code String} to convert.
     *
     * @return the Boolean value of the string, {@code null} if the input is
     *      {@code null} or if text cannot be reasonably converted.
     */
    public static Boolean asBoolean(final String value) {
        final String s;

        if (isEmpty(s = trimToEmpty(value).toLowerCase())) {
            return null;
        }

        final Boolean b = toBooleanObject(s);

        // If the String was converted to a Boolean then return that value.
        if (b != null) {
            return b;
        }

        // If {@link BooleanUtils#toBooleanObject} cannot convert the String
        // value to a Boolean then it returns null.  If it does return null
        // then compare the String against other values.

        // Check for more alternatives to TRUE.
        if (equalsAny(s, "1", "active", "activated", "enable", "enabled", "ack",
            "go", "alive", "run", "running", "success", "successful", "open",
            "opened", "successfully", "complete",
            ":-)", ":)", "(^.^)", "(^^)", "^^", ":-]", ":]", ":-3", ":3",
            ":->", ":>", "<3", "\\o/", ":-d", ":d", "8-d", "8d", "^5", "o/\\o",
            ">_>^ ^<_<")) {
            return Boolean.TRUE;
        }

        // Check for more alternatives to FALSE.
        if (equalsAny(s, "0", "no", "null", "nil", "undefined", "inactive",
            "deactivate", "deactivated", "disable", "disabled", "nak",
            "waiting", "blocked", "terminated", "halt", "halted", "pause",
            "paused", "suspended", "sleep", "sleeping", "asleep", "stop",
            "stopped", "cancel", "canceled", "cancelled", "fail", "failed",
            "failure", "error", "fatal", "exception", "close", "closed",
            ":-(", ":(", "(t_t)", "t_t", "tt", "</3", "<\\3", "(>_<)",
            "(>_<)>", ">:(", "sto", "otz", "otl", "orz" )) {
            return Boolean.FALSE;
        }

        // No match found.

        Utility.logger.warn(
            "Unable to convert '{}' to a Boolean; returning null",
            value);

        return null;
    }

    /**
     * Null-safe method that returns the {@code boolean} value of a
     * {@code Boolean} instance.  Note, {@code null} evaluates to {@code false}.
     *
     * @param value A {@code Boolean} or {@code null}.
     *
     * @return The {@code boolean} value of the provided {@code Boolean};
     *      {@code null} returns {@code false}.
     */
    public static boolean booleanValue(final Boolean value) {
        return (value == null)
            ? false
            : value.booleanValue();
    }

    /**
     * Returns a {@code boolean} equivalent of the provided {@code String}.
     *
     * @param value A {@code String} holding a textual representation of a value
     *      that can be converted to a {@code boolean}.
     *
     * @return {@code true} if the parameter can be correlated to the boolean
     *      value {@code true}, {@code false} if the parameter can be correlated
     *      to {@code false} or otherwise.
     */
    public static boolean booleanValue(final String value) {
        return Utility.booleanValue(Utility.asBoolean(value));
    }

    /**
     * Converts the data accessible via the passed in {@code Iterable} into a
     * textual representation.  A separator between each pair of elements.
     *
     * @param iterable An {@code Iterable} to a set of underlying data.
     *
     * @return A textual representation of the data that is accessible via the
     *      supplied {@code Iterable}.
     */
    public static String asString(final Iterable<?> iterable) {
        // Null or no "next" item.
        if ((iterable == null) || !iterable.iterator().hasNext()) {
            return "{}";
        }

        // Return a String generated by traversing all items.
        return "{" + join(iterable, Utility.DEFAULT_SEPARATOR) + "}";
    }

    /**
     * Converts the data, key-value pair(s), in a provided {@code Map} into a
     * textual representation.  A separator between each key-value pair.
     *
     * @param map A {@code Map} of data.
     *
     * @return A textual representation of the data in the provided {@code Map}.
     */
    public static String asString(final Map<?,?> map) {
        // Null or empty map.
        if (isEmpty(map)) {
            return "{}";
        }

        // Return a String generated by traversing all key-value pairs.
        return "{" + join(map.entrySet(), Utility.DEFAULT_SEPARATOR) + "}";
    }

    /**
     * Returns a {@code String} object representing this {@code Number}'s value.
     *
     * @param value The {@code Number} to return the {@code String} value.
     *
     * @return A {@code String} equivalent of the numeric value.
     */
    public static String asString(final Number value) {
        return (value == null) ? "" : String.valueOf(value);
    }

    /**
     * Converts the data in the passed in {@code array} into a textual
     * representation.  A separator between each pair of elements.
     *
     * @param values An {@code array} of items.
     *
     * @return A textual representation of the data in the {@code array}.
     */
    public static String asString(final Object[] values) {
        // Null or empty array.
        if (isEmpty(values)) {
            return "[]";
        }

        // Return a String generated by traversing all items.
        return "[" + join(values, Utility.DEFAULT_SEPARATOR) + "]";
    }

    /**
     * Add quotation marks or apostrophes to the passed in value.
     *
     * @param value The {@code String} value to quote.
     *
     * @return The original value with quotes added.
     */
    public static String quote(final String value) {
        if (isEmpty(value)) {
            return "\"\"";
        }

        final String s = trimToEmpty(value);

        if (s.startsWith("\"") || s.endsWith("\"")) {
            return "'" + value + "'";
        }

        return "\"" + value + "\"";
    }

    /**
     * Add apostrophes or quotation marks to the passed in value.
     *
     * @param value The {@code String} value to quote.
     *
     * @return The original value with "single-quotes" (apostrophes) added.
     */
    public static String singleQuote(final String value) {
        if (isEmpty(value)) {
            return "''";
        }

        final String s = trimToEmpty(value);

        if (s.startsWith("'") || s.endsWith("'")) {
            return "\"" + value + "\"";
        }

        return "'" + value + "'";
    }

    /**
     * Add quotation marks or apostrophes to the passed in value if needed.
     * Quotation marks are added if the value contains a quote or any external
     * whitespace characters.
     *
     * @param value The {@code String} value to quote.
     *
     * @return The original value with quotes added if needed or the original
     *      value if not.
     */
    public static String quoteIfNeeded(final String value) {
        // Empty string value so no quotes needed.
        if (isEmpty(value)) {
            return "";
        }

        // Perform checks using a version of the passed in value that has had
        // any external whitespace characters removed.
        final String s = trimToEmpty(value);

        // The value is already enclosed in quotes or "single-quotes"
        // (apostrophes) so no quotes needed.
        if ((s.startsWith("\"") && s.endsWith("\""))
            || (s.startsWith("'") && s.endsWith("'"))) {
            return value;
        }

        // The value contains at least one quotation mark but does not begin and
        // end with one.  Wrap in "single-quotes" (apostrophes).
        if (s.contains("\"")) {
            return Utility.singleQuote(value);
        }

        // The value contains one of the characters commonly used as a separator
        // in CSV files then wrap in quotes.
        if (s.contains(",") || s.contains("|")) {
            return Utility.quote(value);
        }

        // If value has external whitespace (assumed to be significant) then
        // wrap in quotes to keep, otherwise return original value.
        return (value.length() > s.length())
            ? Utility.quote(value)
            : value;
    }

    /**
     * Creates a {@code ZonedDateTime} instance from a {@code Date}.
     *
     * @param value A {@code Date}.
     *
     * @return A {@code ZonedDateTime} instance.
     *
     * @throws IllegalArgumentException if the parameter is null.
     */
    public static ZonedDateTime toDateTime(final Date value)
    throws IllegalArgumentException {
        if (value == null) {
            throw new IllegalArgumentException("Invalid/null date");
        }

        return ZonedDateTime.from(value.toInstant());
    }

    /**
     * Compares two strings lexicographically.  The comparison is based on the
     * Unicode value of each character in the strings.  The result is a negative
     * integer if the first {@code String} lexicographically precedes the
     * second {@code String} argument. The result is a positive integer if
     * the first {@code String} argument lexicographically follows the second.
     * The result is zero if the {@code Strings} are equal.
     *
     * @param one the first {@code String} to be compared.
     * @param two the second {@code String} to be compared.
     *
     * @return The value 0 if the arguments are equal; a value less than 0 if
     *      the first {@code String} is lexicographically less than the second;
     *      and a value greater than 0 if the first {@code String} argument is
     *      lexicographically greater than the second.
     */
    public static int compareTo(final String one, final String two) {
        // Both references are either to the same object or both are null.
        if (one == two) {
            return 0;
        }

        // One or the other is null...
        if (one == null) {
            return -1;
        } else if (two == null) {
            return 1;
        }

        // Neither is null.
        int result = one.compareToIgnoreCase(two);

        // If comparing without regard to case returned 0, the compare again
        // this take using case.  This double comparison prevents "ZED" from
        // preceding "ant" since all lower case characters lexicographically
        // follow the upper case ones.
        return (result == 0)
            ? one.compareTo(two)
            : result;
    }

    public static String truncateAndPrefix(final String prefix,
        final String value, final int length) {
        if (StringUtils.length(value) <= length) {
            return value;
        }

        final int prefixLength = StringUtils.length(prefix);

        if (prefixLength > 0) {
            return prefix
                + StringUtils.right(value, Math.max(0, length - prefixLength));
        }

        return StringUtils.right(value, length);
    }

    public static String truncateAndPrefix(final String value,
        final int length) {
        return Utility.truncateAndPrefix("...", value, length);
    }

    public static synchronized String datePrefix(final String value) {
        return Utility.dateFormatter.format(new Date())
            + ":" + trimToEmpty(value);
    }

    /**
     * Private no-argument constructor.  Prevents sub-classing and prevents an
     * instance of this class from being instantiated.
     */
    private Utility() {
        super();
    }
}

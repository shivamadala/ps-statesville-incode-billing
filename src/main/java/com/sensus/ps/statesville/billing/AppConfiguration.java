package com.sensus.ps.statesville.billing;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.ExtractorLineAggregator;
import org.springframework.batch.item.file.transform.FieldExtractor;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.item.support.PassThroughItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.sensus.ps.statesville.billing.batch.DeleteFilesJobListener;
import com.sensus.ps.statesville.billing.batch.JobHolder;
import com.sensus.ps.statesville.billing.batch.MeterFileProcessor;
import com.sensus.ps.statesville.billing.batch.MeterInboundFileDBWriter;
import com.sensus.ps.statesville.billing.batch.MeterInboundFileReader;
import com.sensus.ps.statesville.billing.batch.MeterInboundReportWriter;
import com.sensus.ps.statesville.billing.batch.MeterInboundTransformDBWriter;
import com.sensus.ps.statesville.billing.batch.MeterInboundTransformProcessor;
import com.sensus.ps.statesville.billing.batch.MeterInboundTransformReader;
import com.sensus.ps.statesville.billing.batch.MeterOutboundFileDBWriter;
import com.sensus.ps.statesville.billing.batch.MeterOutboundFileReader;
import com.sensus.ps.statesville.billing.batch.MeterOutboundReportWriter;
import com.sensus.ps.statesville.billing.batch.MeterOutboundTransformProcessor;
import com.sensus.ps.statesville.billing.batch.MeterOutboundTransformReader;
import com.sensus.ps.statesville.billing.batch.MoveFilesJobListener;
import com.sensus.ps.statesville.billing.model.InboundMeter;
import com.sensus.ps.statesville.billing.model.OutboundInputMeter;
import com.sensus.ps.statesville.billing.model.OutboundMeter;

/**
 * Configuration class for this application.
 */
@SuppressWarnings("javadoc")
@Configuration
@EnableBatchProcessing
public class AppConfiguration {

	/**
	 * Logs messages from AppConfiguration methods.
	 */
	private static final Logger logger = LoggerFactory.getLogger(AppConfiguration.class);
	
	/**
	 * Set of inbound input files to read (wildcards allowed).
	 */
	@Value("file:${ps.statesville.client.input.directory}/STSVL_VFLEX_WE_*.csv")
	private Resource[] inputInboundResources = null;

	/**
	 * Set of outbound input files to read (wildcards allowed).
	 */
	@Value("file:${ps.statesville.client.input.directory}/Billing_Meter_Export*.txt")
	private Resource[] inputOutboundResources = null;


	/**
	 * {@code DataSource} for connecting to the application's H2 database.
	 *
	 * @return A {@code DataSource)
	 */
	@Primary
	@Bean("MeterDataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource getMeterDataSource() {
		return DataSourceBuilder.create().build();
	}

	/**
	 * Returns a reader that reads the CSV input file and emits {@code Meter}s.
	 *
	 * @param inFilePath The path to the CSV input file.
	 *
	 * @return A {@code MeterInboundFileReader}.
	 */
	@Bean("meterInboundFileReader")
	@Scope("prototype")
	@Value("${ps.statesville.client.input.file}")
	public MeterInboundFileReader meterInboundFileReader(final String inFilePath) {
		return new MeterInboundFileReader(inFilePath);
	}

	/**
	 * Returns a reader that reads the CSV input file and emits {@code Meter}s.
	 *
	 * @param inFilePath The path to the CSV input file.
	 *
	 * @return A {@code MeterOutboundFileReader}.
	 */
	@Bean("meterOutboundFileReader")
	@Scope("prototype")
	@Value("${ps.statesville.client.input.file}")
	public MeterOutboundFileReader meterOutboundFileReader(final String outFilePath) {
		return new MeterOutboundFileReader(outFilePath);
	}

	/**
	 * Returns a processor that inputs {@code Meters} read from the a CSV input file
	 * and emits updated {@code Meter}s.
	 *
	 * @return A {@code MeterFileProcessor}.
	 */
	@Bean("meterFileProcessor")
	@Scope("prototype")
	public MeterFileProcessor meterFileProcessor() {
		return new MeterFileProcessor();
	}

	/**
	 * A writer that inputs {@code InboundMeter}s and then writes them to a
	 * data-store.
	 *
	 * @return A {@code MeterInboundFileDBWriter}.
	 */
	@Bean("meterInboundFileDBWriter")
	@Scope("prototype")
	public MeterInboundFileDBWriter meterInboundFileDBWriter() {
		return new MeterInboundFileDBWriter();
	}

	/**
	 * A writer that inputs {@code OutboundMeter}s and then writes them to a
	 * data-store.
	 *
	 * @return A {@code MeterOutboundFileDBWriter}.
	 */
	@Bean("meterOutboundFileDBWriter")
	@Scope("prototype")
	public MeterOutboundFileDBWriter meterOutboundFileDBWriter() {
		return new MeterOutboundFileDBWriter();
	}

	/**
	 * A Spring Batch {@code JobExecutionListener} that moves the files from a
	 * source directory to a destination directory after the successful conclusion
	 * of a {@code Job}. Only moves files that have the ".csv" extension.
	 *
	 * @param inPath  The source directory.
	 * @param outPath The destination directory where the files are to be moved.
	 *
	 * @return A {@code JobExecutionListener}.
	 */
	@Bean("moveFilesJobListener")
	@Scope("prototype")
	public MoveFilesJobListener moveFilesJobListener() {
		return new MoveFilesJobListener();
	}

	/**
	 * A Spring Batch {@code JobExecutionListener} that deletes the files from a
	 * source directory after the successful conclusion
	 * of a {@code Job}. Only deletes files that have the extension.
	 *
	 * @param inPath  The source directory.
	 *
	 * @return A {@code JobExecutionListener}.
	 */
	@Bean("deleteFilesJobListener")
	@Scope("prototype")
	public DeleteFilesJobListener deleteFilesJobListener() {
		return new DeleteFilesJobListener();
	}

	@Bean("meterInboundTransformReader")
	@Scope("prototype")
	public MeterInboundTransformReader meterInboundTransformReader(final EntityManagerFactory factory) {
		final MeterInboundTransformReader reader = new MeterInboundTransformReader();

		reader.setEntityManagerFactory(factory);
		reader.setPageSize(100);

		return reader;
	}

	@Bean("meterOutboundTransformReader")
	@Scope("prototype")
	public MeterOutboundTransformReader meterOutboundTransformReader(final EntityManagerFactory factory) {
		final MeterOutboundTransformReader reader = new MeterOutboundTransformReader();

		reader.setEntityManagerFactory(factory);
		reader.setPageSize(100);

		return reader;
	}

	/**
	 * Returns a processor that inputs {@code Meters} read from the a CSV input file
	 * and emits updated {@code Meter}s.
	 *
	 * @return A {@code MeterInboundTransformProcessor}.
	 */
	@Bean("meterInboundTransformProcessor")
	@Scope("prototype")
	public MeterInboundTransformProcessor meterInboundTransformProcessor() {
		return new MeterInboundTransformProcessor();
	}

	/**
	 * Returns a processor that inputs {@code Meters} read from the a CSV input file
	 * and emits updated {@code Meter}s.
	 *
	 * @return A {@code MeterOutboundTransformProcessor}.
	 */
	@Bean("meterOutboundTransformProcessor")
	@Scope("prototype")
	public MeterOutboundTransformProcessor meterOutboundTransformProcessor() {
		return new MeterOutboundTransformProcessor();
	}

	/**
	 * A writer that inputs {@code Meter}s and then writes them to a data-store.
	 *
	 * @return A {@code MeterInboundTransformDBWriter}.
	 */
	@Bean("meterInboundTransformDBWriter")
	@Scope("prototype")
	public MeterInboundTransformDBWriter meterInboundTransformDBWriter() {
		return new MeterInboundTransformDBWriter();
	}

	@Bean("itemProcessor")
	@Scope("prototype")
	public ItemProcessor itemProcessor() {
		return new MeterOutboundTransformProcessor();
	}

	@Bean("meterInboundReportWriter")
	@Scope("prototype")
	@Value("${ps.statesville.client.output.directory}")
	public MeterInboundReportWriter meterInboundReportWriter(final String outFilePath) {
		final MeterInboundReportWriter writer = new MeterInboundReportWriter();
		writer.setOutFilePath(outFilePath);

		writer.setResource(new FileSystemResource(outFilePath + "/track.out"));

		final ExtractorLineAggregator<InboundMeter> lineAggregator = new DelimitedLineAggregator<>();
		final BeanWrapperFieldExtractor<InboundMeter> fieldExtractor = new BeanWrapperFieldExtractor<>();

		fieldExtractor.setNames(new String[] {

				"customBillingCycle", "accountBillingCycle", "accountId", "accountStatus", "customServiceCode",
				"customSlot", "customerName", "assetAddress", "assetCity", "assetState", "assetZip", "securityToken",
				"numberOfDials", "meterId2", "meterId", "ertId", "lastKnownRead", "commodity", "meterManufacturer",
				"accountServiceType", "accountRateCode", "displayMultiplier", "customMeterSize", "customIncodeLatitude",
				"customIncodeLongitude", "customDeviceStatus", "lastBilledDate", "minimumUsageThreshold",
				"maximumUsageThreshold", "customLocationNumber", "customLastDemandRead", "customLastKvarRead",
				"customLastGeneratedRead", "offpeakSlot", "offpeakServiceCode", "offpeakMeterId"

		});

		lineAggregator.setFieldExtractor(fieldExtractor);

		writer.setLineAggregator(lineAggregator);
		writer.setShouldDeleteIfEmpty(false);
		writer.setShouldDeleteIfExists(true);

		return writer;
	}

	@Bean("meterOutboundReportWriter")
	@Scope("prototype")
	@Value("${ps.statesville.client.output.directory}")
	public MeterOutboundReportWriter meterOutboundReportWriter(final String outFilePath) {
		final MeterOutboundReportWriter writer = new MeterOutboundReportWriter();
		writer.setOutFilePath(outFilePath);

		writer.setResource(new FileSystemResource(outFilePath + "/track.out"));

		final LineAggregator<List<OutboundMeter>> lineAggregator = new DelimitedLineAggregator<>();
		final FieldExtractor<List<OutboundMeter>> fieldExtractor = new BeanWrapperFieldExtractor<>();

		((BeanWrapperFieldExtractor<List<OutboundMeter>>) fieldExtractor).setNames(new String[] {

				"accountId", "serviceCode", "serviceSlot", "nonTouKwhRead", "nonTouKwhReadDate", "nonTouKwhReadTime",
				"incodeLatitude", "incodeLongitude", "nonTouKwRead", "nonTouKwReadDate", "nonTouKwReadTime",
				"touTier1Read", "touTier1ReadDate", "touTier1ReadTime"

		});

		((ExtractorLineAggregator<List<OutboundMeter>>) lineAggregator).setFieldExtractor(fieldExtractor);

		writer.setLineAggregator(lineAggregator);
		writer.setShouldDeleteIfEmpty(false);
		writer.setShouldDeleteIfExists(true);

		return writer;
	}

	@SuppressWarnings("javadoc")
	@Bean("job-inbound-load-transform-report")
	@Scope("prototype")
	public Job jobInboundLoadMeters(final JobBuilderFactory jobFactory, final StepBuilderFactory stepFactory,
			final MeterInboundFileReader meterInboundFileReader, final MoveFilesJobListener moveCsvFilesJobListener,
			final MoveFilesJobListener moveDbFilesJobListener, final DeleteFilesJobListener deleteFilesJobListener,
			final MeterInboundFileDBWriter meterInboundFileDBWriter,
			final MeterInboundTransformReader meterInboundTransformReader,
			final MeterInboundTransformProcessor meterInboundTransformProcessor,
			final MeterInboundTransformDBWriter meterInboundTransformDBWriter,
			final MeterInboundTransformReader meterInboundTranformReader,
			final MeterInboundReportWriter meterInboundReportWriter,
			@Value("${ps.statesville.client.input.directory}") final String inPath,
			@Value("${ps.statesville.client.archive.directory}") final String outPath,
			@Value("${ps.statesville.db.output.directory}") final String inDBPath) {

		MultiResourceItemReader<InboundMeter> resourceItemReader = new MultiResourceItemReader<>();

		resourceItemReader.setResources(this.inputInboundResources);
		resourceItemReader.setDelegate(meterInboundFileReader);
		resourceItemReader.setStrict(false);

		moveCsvFilesJobListener.setArchiveDirectory(outPath);
		moveCsvFilesJobListener.setInputDirectory(inPath);
		moveCsvFilesJobListener.setFileExtension(".csv");

		@StepScope
		final Step loadMeterStep = stepFactory.get("step-load-csv")
		.<InboundMeter, InboundMeter>chunk(100)
				.reader(resourceItemReader)
				.processor(new PassThroughItemProcessor<InboundMeter>())
				.writer(meterInboundFileDBWriter)
				.build();

		final Step transformMeterStep = stepFactory.get("step-transform-meter")
				.<InboundMeter, InboundMeter>chunk(100)
				.reader(meterInboundTransformReader)
				.processor(meterInboundTransformProcessor)
				.writer(meterInboundTransformDBWriter)
				.build();

		deleteFilesJobListener.setInputDirectory(inDBPath);
		deleteFilesJobListener.setFileExtension(".db");

		@StepScope
		final Step reportStep = stepFactory.get("step-report-meter")
				.<InboundMeter, InboundMeter>chunk(100)
				.reader(meterInboundTranformReader)
				.processor(new PassThroughItemProcessor<InboundMeter>())
				.writer(meterInboundReportWriter)
				.build();

		return jobFactory.get("job-inbound-load-transform-report")
				.incrementer(new RunIdIncrementer())
				.start(loadMeterStep)
				.listener(moveCsvFilesJobListener)
				.next(transformMeterStep)
				.next(reportStep)
				.listener(deleteFilesJobListener)
				.build();
	}

	@SuppressWarnings("unchecked")
	@Bean("job-outbound-load-transform-report")
	@Scope("prototype")
	public Job jobOutboundLoadMeters(final JobBuilderFactory jobFactory, final StepBuilderFactory stepFactory,
			final MeterOutboundFileReader meterOutboundFileReader, final MoveFilesJobListener moveCsvFilesJobListener,
			final MeterOutboundFileDBWriter meterOutboundFileDBWriter,
			final DeleteFilesJobListener deleteFilesJobListener,
			final MeterOutboundTransformReader meterOutboundTransformReader,
			final ItemProcessor<? super OutboundInputMeter, ? extends List<OutboundMeter>> meterOutboundTransformProcessor,
			final MeterOutboundReportWriter meterOutboundReportWriter,
			@Value("${ps.statesville.client.input.directory}") final String inPath,
			@Value("${ps.statesville.client.archive.directory}") final String outPath,
			@Value("${ps.statesville.db.output.directory}") final String inDBPath) {

		MultiResourceItemReader<OutboundInputMeter> resourceItemReader = new MultiResourceItemReader<>();

		resourceItemReader.setResources(this.inputOutboundResources);
		resourceItemReader.setDelegate(meterOutboundFileReader);
		resourceItemReader.setStrict(false);

		moveCsvFilesJobListener.setArchiveDirectory(outPath);
		moveCsvFilesJobListener.setInputDirectory(inPath);
		moveCsvFilesJobListener.setFileExtension(".txt");
		
		@StepScope
		final Step loadMeterStep = stepFactory.get("step-load-csv")
				.<OutboundInputMeter, OutboundInputMeter>chunk(100)
				.reader(resourceItemReader)
				.processor(new PassThroughItemProcessor<OutboundInputMeter>())
				.writer(meterOutboundFileDBWriter)
				.build();

		deleteFilesJobListener.setInputDirectory(inDBPath);
		deleteFilesJobListener.setFileExtension(".db");

		@StepScope
		final Step reportStep = stepFactory.get("step-report-meter")
				.<OutboundInputMeter, List<OutboundMeter>>chunk(100)
				.reader(meterOutboundTransformReader)
				.processor(meterOutboundTransformProcessor)
				.writer(meterOutboundReportWriter)
				.build();

		return jobFactory.get("job-outbound-load-transform-report")
				.incrementer(new RunIdIncrementer())
				.start(loadMeterStep)
				.listener(moveCsvFilesJobListener)
				.next(reportStep)
				.listener(deleteFilesJobListener)
				.build();
	}

	@Bean("jobHolder")
	@Scope("prototype")
	public JobHolder jobHolder() {
		return new JobHolder();
	}
}
